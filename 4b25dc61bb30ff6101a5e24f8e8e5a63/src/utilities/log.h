#ifndef LOG_H
#define LOG_H

#include <android/log.h>

void LogFile(const char* format, ...);

#define LOG(...) \ 
    __android_log_print(ANDROID_LOG_INFO, "AXL", __VA_ARGS__); 

#define VERBOSE(...) __android_log_print(ANDROID_LOG_VERBOSE, "AXL", __VA_ARGS__)
#define WARN(...) __android_log_print(ANDROID_LOG_WARN, "AXL", __VA_ARGS__)
#define ERROR(...) __android_log_print(ANDROID_LOG_ERROR, "AXL", __VA_ARGS__)
#define FATAL(...) __android_log_print(ANDROID_LOG_FATAL, "AXL", __VA_ARGS__)

#endif // LOG_H
