/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * @author Aly Cardinal
 * @link https://vk.com/id650525154 
 * @community https://vk.com/a.home846
 * 
*/

#include "../main.h"

jobject javawrapper::activity;
JavaVM* javawrapper::globalVM;

jmethodID javawrapper::s_DisableUIStuff;
jmethodID javawrapper::s_HideLayoutCut;
jmethodID javawrapper::s_HideNavBar;
jmethodID javawrapper::s_ShowMessage;
jmethodID javawrapper::s_MakeDialog;

void javawrapper::setGlobalVM(JavaVM* vm)
{
    javawrapper::globalVM = vm;
}

JNIEnv* javawrapper::getEnv()
{
	if(!javawrapper::globalVM) {
		return 0;
	}

	JNIEnv* env = nullptr;
	int getEnvStat = javawrapper::globalVM->GetEnv((void**)&env, JNI_VERSION_1_4);

	if (getEnvStat == JNI_EDETACHED)
	{
		ERROR("JNI Env: not attached");
		
		if (javawrapper::globalVM->AttachCurrentThread(&env, NULL) != 0)
		{
			ERROR("Failed to attach");
			return nullptr;
		}
	}
	if (getEnvStat == JNI_EVERSION)
	{
		ERROR("JNI Env: version not supported");
		return nullptr;
	}

	if (getEnvStat == JNI_ERR)
	{
		ERROR("JNI Env: error");
		return nullptr;
	}

	return env;
}

void javawrapper::initialize(JNIEnv* env, jobject activity)
{
    javawrapper::activity = env->NewGlobalRef(activity);

	jclass nvEventClass = env->GetObjectClass(activity);
	if (!nvEventClass)
	{
		ERROR("nvEventClass not found!");
		return;
	}

	javawrapper::s_DisableUIStuff = env->GetMethodID(nvEventClass, "disableUIStuff", "()V");
    javawrapper::s_HideLayoutCut = env->GetMethodID(nvEventClass, "hideLayoutCut", "()V");
    javawrapper::s_HideNavBar = env->GetMethodID(nvEventClass, "hideNavBar", "()V");
    javawrapper::s_ShowMessage = env->GetMethodID(nvEventClass, "sendToast", "(Ljava/lang/String;)V");
    javawrapper::s_MakeDialog = env->GetMethodID(nvEventClass, "makeDialog", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V");

	env->DeleteLocalRef(nvEventClass);
}

void javawrapper::uninitialize()
{
	JNIEnv* pEnv = javawrapper::getEnv();
	if (pEnv) {
		pEnv->DeleteGlobalRef(javawrapper::activity);
	}
}

void javawrapper::disableUIStuff()
{
    JNIEnv* env = javawrapper::getEnv();

	if (!env)
	{
		ERROR("No env");
		return;
	}

	env->CallVoidMethod(activity, javawrapper::s_DisableUIStuff);

	EXCEPTION_CHECK(env);
}

void javawrapper::hideNavBar()
{
    JNIEnv* env = javawrapper::getEnv();

	if (!env)
	{
		ERROR("No env");
		return;
	}

	env->CallVoidMethod(activity, javawrapper::s_HideNavBar);

	EXCEPTION_CHECK(env);
}

void javawrapper::hideLayoutCut()
{
    JNIEnv* env = javawrapper::getEnv();

	if (!env)
	{
		ERROR("No env");
		return;
	}

	env->CallVoidMethod(activity, javawrapper::s_HideLayoutCut);

	EXCEPTION_CHECK(env);
}

void javawrapper::showMessage(const char* szMsg)
{
    JNIEnv* env = javawrapper::getEnv();

	if (!env)
	{
		ERROR("No env");
		return;
	}

    jstring jStr = env->NewStringUTF(szMsg);
	env->CallVoidMethod(activity, javawrapper::s_ShowMessage, jStr);

	EXCEPTION_CHECK(env);
}

void javawrapper::makeDialog(const char* szTitle, const char* szMsg, const char* szButton1, const char* szButton2)
{
    JNIEnv* env = javawrapper::getEnv();

	if (!env)
	{
		ERROR("No env");
		return;
	}

    jstring jTitle = env->NewStringUTF(szTitle);
    jstring jMsg = env->NewStringUTF(szMsg);
    jstring jButton1 = env->NewStringUTF(szButton1);
    jstring jButton2 = env->NewStringUTF(szButton2);
	env->CallVoidMethod(activity, javawrapper::s_MakeDialog, jTitle, jMsg, jButton1, jButton2);

	EXCEPTION_CHECK(env);
}