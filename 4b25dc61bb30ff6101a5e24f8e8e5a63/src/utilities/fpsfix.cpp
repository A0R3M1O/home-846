/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * @author Aly Cardinal
 * @link https://vk.com/id650525154 
 * @community https://vk.com/a.home846
 * 
*/

#include "../main.h"
#include "fpsfix.h"
#include <sys/syscall.h>

static void setThreadAffinityMask(pid_t tid, uint32_t mask)
{
	if (syscall(__NR_sched_setaffinity, tid, sizeof(mask), &mask))
	{
		LOG("Could not set thread affinity: mask=0x%x err=0x%x", mask, errno);
	}
}

void CFPSFix::Routine()
{
	while (true)
	{
		m_Mutex.lock();

		for (auto& i : m_Threads)
		{
			uint32_t mask = 0xff;

			setThreadAffinityMask(i, mask);
		}
		
		m_Mutex.unlock();

		std::this_thread::sleep_for(std::chrono::milliseconds(5000));
	}
}

CFPSFix::CFPSFix()
{
	std::thread(&CFPSFix::Routine, this).detach();
}

CFPSFix::~CFPSFix()
{
}

void CFPSFix::PushThread(pid_t tid)
{
	std::lock_guard<std::mutex> lock(m_Mutex);

	m_Threads.push_back(tid);
}
