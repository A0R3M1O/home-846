/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * @author Aly Cardinal
 * @link https://vk.com/id650525154 
 * @community https://vk.com/a.home846
 * 
*/

#include "main.h"

uint32_t utilities::getTickCount()
{
	struct timeval tv;
	gettimeofday(&tv, nullptr);
	return (tv.tv_sec*1000+tv.tv_usec/1000);
}

uintptr_t utilities::getLibraryHandle(const char* library)
{
    char filename[0xFF] = {0},
    buffer[2048] = {0};
    FILE *fp = 0;
    uintptr_t address = 0;

    sprintf( filename, "/proc/%d/maps", getpid() );

    fp = fopen( filename, "rt" );
    if(fp == 0)
    {
        goto done;
    }

    while(fgets(buffer, sizeof(buffer), fp))
    {
        if( strstr( buffer, library ) )
        {
            address = (uintptr_t)strtoul( buffer, 0, 16 );
            break;
        }
    }

    done:

    if(fp)
      fclose(fp);

    return address;
}

uintptr_t utilities::findMethod(const char *libname, const char *name)
{
    auto result = dlsym(dlopen(libname, RTLD_NOW), name);

    Dl_info info;
    dladdr((void*)result, &info);

    return ((uintptr_t)info.dli_saddr - utilities::getLibraryHandle(libname));
}

char* utilities::g_szStorage = nullptr;
void utilities::setClientStorage(char* storage)
{
    LOG("Storage: %s", storage);
    utilities::g_szStorage = storage;
}

char* utilities::getClientStorage()
{
    return utilities::g_szStorage;
}

uintptr_t HOOK_GetTexture(const char* a1);
uintptr_t utilities::loadTextureFromDB(const char* dbname, const char *texture)
{
    // TextureDatabaseRuntime::GetDatabase(dbname)
	uintptr_t db_handle = (( uintptr_t (*)(const char*))(SA(0x1BF530+1)))(dbname);
	if(!db_handle)
	{
		LOG("Error: Database not found! (%s)", dbname);
		return 0;
	}
	// TextureDatabaseRuntime::Register(db)
	(( void (*)(uintptr_t))(SA(0x1BE898+1)))(db_handle);
	uintptr_t tex = HOOK_GetTexture(texture);

	if(!tex) LOG("Error: Texture (%s) not found in database (%s)", dbname, texture);

	// TextureDatabaseRuntime::Unregister(db)
	(( void (*)(uintptr_t))(SA(0x1BE938+1)))(db_handle);

	return tex;
}

uintptr_t utilities::loadTextureFromTxd(const char* database, const char *texture)
{
	uintptr_t pRwTexture = 0;

	int g_iTxdSlot = ((int (__fastcall *)(const char *))(SA(0x55BB85)))(database); // CTxdStore::FindTxdSlot
	if(g_iTxdSlot == -1)
	{
		LOG("no txd");
	}
	else
	{
		((void (*)(void))(SA(0x55BD6D)))(); // CTxdStore::PushCurrentTxd
		((void (__fastcall *)(int, uint32_t))(SA(0x55BCDD)))(g_iTxdSlot, 0);
		((void (__fastcall *)(uintptr_t *, const char *))(SA(0x551855)))(&pRwTexture, texture); // CSprite2d::SetTexture
		((void (*)(void))(SA(0x55BDA9)))(); // CTxdStore::PopCurrentTxd
	}

	return pRwTexture;
}