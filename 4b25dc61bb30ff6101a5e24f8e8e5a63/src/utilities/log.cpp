#include "main.h"
#include "log.h"

void LogFile(const char* format, ...)
{
    if(!utilities::getClientStorage()) {
        return;
    }

    char info[512] = {0};
    va_list arg;
	va_start(arg, format);
	vsnprintf(info, sizeof(info), format, arg);
	va_end(arg);

    char path[0xFF] = {0};
	FILE* fileLog = nullptr;

	sprintf(path, "%sMTASA/debug.log", utilities::getClientStorage());
	fileLog = fopen(path, "a");

	if(fileLog == nullptr) {
        return;
    }

	fprintf(fileLog, "%s\n", info);
	fflush(fileLog);
    fclose(fileLog);
}