/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * @author Aly Cardinal
 * @link https://vk.com/id650525154 
 * @community https://vk.com/a.home846
 * 
*/

#include "main.h"
#include "armhook.h"
#include "log.h"

#include <map>
#include <set>

struct SCrashAvertedInfo
{
    uint uiTickCount;
    int  uiUsageCount;
};

static uint ms_uiInCrashZone = 0;
static std::map<int, SCrashAvertedInfo>    ms_CrashAvertedMap;

void crashdump::OnCrashAverted(uint uiId)
{
	if(!&ms_CrashAvertedMap[uiId]) {
		ms_CrashAvertedMap[uiId].uiUsageCount = 0;
	}

	ms_CrashAvertedMap[uiId].uiTickCount = utilities::getTickCount();
	ms_CrashAvertedMap[uiId].uiUsageCount++;
}

void crashdump::OnEnterCrashZone(uint id)
{
	ms_uiInCrashZone = id;
}

void handler1(int signum, siginfo_t *info, void* contextPtr)
{
	ucontext* context = (ucontext_t*)contextPtr;

	if(info->si_signo == SIGSEGV) {
		crashdump::report(context, info, "SIGSEGV");
	}

	return;
}

void handler2(int signum, siginfo_t *info, void* contextPtr)
{
	ucontext* context = (ucontext_t*)contextPtr;

	if(info->si_signo == SIGABRT) {
		crashdump::report(context, info, "SIGABRT");
	}

	return;
}

void handler3(int signum, siginfo_t *info, void* contextPtr)
{
	ucontext* context = (ucontext_t*)contextPtr;

	if(info->si_signo == SIGFPE) {
		crashdump::report(context, info, "SIGFPE");
	}

	return;
}

void handler4(int signum, siginfo_t *info, void* contextPtr)
{
	ucontext* context = (ucontext_t*)contextPtr;

	if(info->si_signo == SIGBUS) {
		crashdump::report(context, info, "SIGBUS");
	}

	return;
}

void handler5(int signum, siginfo_t *info, void* contextPtr)
{
	ucontext* context = (ucontext_t*)contextPtr;

	if(info->si_signo == SIGILL) {
		crashdump::report(context, info, "SIGILL");
	}

	return;
}

void handler6(int signum, siginfo_t *info, void* contextPtr)
{
	ucontext* context = (ucontext_t*)contextPtr;

	if(info->si_signo == SIGPIPE) {
		crashdump::report(context, info, "SIGPIPE");
	}

	return;
}

void handler7(int signum, siginfo_t *info, void* contextPtr)
{
	ucontext* context = (ucontext_t*)contextPtr;

	if(info->si_signo == SIGSTKFLT) {
		crashdump::report(context, info, "SIGSTKFLT");
	}

	return;
}

void crashdump::initialize()
{
    LOG("initializing crashdump..");

    struct sigaction act;
	act.sa_sigaction = handler1;
	sigemptyset(&act.sa_mask);
	act.sa_flags = SA_SIGINFO;
	sigaction(SIGSEGV, &act, 0);
	
	struct sigaction act2;
	act2.sa_sigaction = handler2;
	sigemptyset(&act2.sa_mask);
	act2.sa_flags = SA_SIGINFO;
	sigaction(SIGABRT, &act2, 0);
	
	struct sigaction act3;
	act3.sa_sigaction = handler3;
	sigemptyset(&act3.sa_mask);
	act3.sa_flags = SA_SIGINFO;
	sigaction(SIGFPE, &act3, 0);
	
	struct sigaction act4;
	act4.sa_sigaction = handler4;
	sigemptyset(&act4.sa_mask);
	act4.sa_flags = SA_SIGINFO;
	sigaction(SIGBUS, &act4, 0);
	
	struct sigaction act5;
	act5.sa_sigaction = handler5;
	sigemptyset(&act5.sa_mask);
	act5.sa_flags = SA_SIGINFO;
	sigaction(SIGILL, &act5, 0);
	
	struct sigaction act6;
	act6.sa_sigaction = handler6;
	sigemptyset(&act6.sa_mask);
	act6.sa_flags = SA_SIGINFO;
	sigaction(SIGPIPE, &act6, 0);
	
	struct sigaction act7;
	act7.sa_sigaction = handler7;
	sigemptyset(&act7.sa_mask);
	act7.sa_flags = SA_SIGINFO;
	sigaction(SIGSTKFLT, &act7, 0);
}

void crashdump::printSymbols(void* pc, void* lr)
{
	Dl_info info_pc, info_lr;

	if (dladdr(pc, &info_pc) != 0)
	{
		ERROR("> %s", info_pc.dli_sname);
	}

	if (dladdr(lr, &info_lr) != 0)
	{
		ERROR("> %s", info_lr.dli_sname);
	}
}

void crashdump::report(ucontext* context, siginfo_t *info, const char* typeName)
{
    uintptr_t pGTASALibrary = utilities::getLibraryHandle("libGTASA.so");
    uintptr_t pClientLibrary = utilities::getLibraryHandle("libMTASA_Engine.so");
    uintptr_t pSCAndLibrary = utilities::getLibraryHandle("libSCAnd.so");
	uintptr_t pCSysLibrary = utilities::getLibraryHandle("libc.so");
	uintptr_t pSTDCPPSysLibrary = utilities::getLibraryHandle("libstdc++.so");
	uintptr_t pMSysLibrary = utilities::getLibraryHandle("libm.so");
	uintptr_t pRakNetLibrary = utilities::getLibraryHandle("libraknet.so");
    
	ERROR("Game crashed!");
	ERROR("Crash ID: %d", rand() % 99999);
	
	if(ms_uiInCrashZone != 0) {
		ERROR("Crash zone ID: %d", ms_uiInCrashZone);
	}

	ERROR("SIGNO: %s | Fault address: 0x%X", typeName, info->si_addr);
	ERROR("android_sigaction signal %d (si_code: %d si_errno: %d si_signo: %d)", info, info->si_code, info->si_errno, info->si_signo);

	ERROR("libGTASA.so base address: 0x%X", pGTASALibrary);
	ERROR("libhmta.so base address: 0x%X", pClientLibrary);
	ERROR("libSCAnd.so base address: 0x%X", pSCAndLibrary);
	ERROR("libc.so base address: 0x%X", pCSysLibrary);
	ERROR("libstdc++.so base address: 0x%X", pSTDCPPSysLibrary);
	ERROR("libm.so base address: 0x%X", pMSysLibrary);
	ERROR("libraknet.so base address: 0x%X", pRakNetLibrary);

	ERROR("Lib crashed at:");

	if(pGTASALibrary) {
		ERROR("[libGTASA.so] pc: 0x%X | lr: 0x%X", context->uc_mcontext.arm_pc - pGTASALibrary, context->uc_mcontext.arm_lr - pGTASALibrary);
	}

	if(pClientLibrary) {
		ERROR("[libMTASA_Engine.so] pc: 0x%X | lr: 0x%X", context->uc_mcontext.arm_pc - pClientLibrary, context->uc_mcontext.arm_lr - pClientLibrary);
	}
	
	if(pSCAndLibrary) {
		ERROR("[libSCAnd.so] pc: 0x%X | lr: 0x%X", context->uc_mcontext.arm_pc - pSCAndLibrary, context->uc_mcontext.arm_lr - pSCAndLibrary);
	}

	if(pCSysLibrary) {
		ERROR("[libc.so] pc: 0x%X | lr: 0x%X", context->uc_mcontext.arm_pc - pCSysLibrary, context->uc_mcontext.arm_lr - pCSysLibrary);
	}

	if(pSTDCPPSysLibrary) {
		ERROR("[libstdc++.so] pc: 0x%X | lr: 0x%X", context->uc_mcontext.arm_pc - pSTDCPPSysLibrary, context->uc_mcontext.arm_lr - pSTDCPPSysLibrary);
	}

	if(pMSysLibrary) {
		ERROR("[libm.so] pc: 0x%X | lr: 0x%X", context->uc_mcontext.arm_pc - pMSysLibrary, context->uc_mcontext.arm_lr - pMSysLibrary);
	}

	if(pRakNetLibrary) {
		ERROR("[libraknet.so] pc: 0x%X | lr: 0x%X", context->uc_mcontext.arm_pc - pRakNetLibrary, context->uc_mcontext.arm_lr - pRakNetLibrary);
	}

	ERROR("register states:");
	ERROR("r0: 0x%X, r1: 0x%X, r2: 0x%X, r3: 0x%X", context->uc_mcontext.arm_r0, context->uc_mcontext.arm_r1, context->uc_mcontext.arm_r2, context->uc_mcontext.arm_r3);
  	ERROR("r4: 0x%x, r5: 0x%x, r6: 0x%x, r7: 0x%x", context->uc_mcontext.arm_r4, context->uc_mcontext.arm_r5, context->uc_mcontext.arm_r6, context->uc_mcontext.arm_r7);
  	ERROR("r8: 0x%x, r9: 0x%x, r10: 0x%x, fp: 0x%x", context->uc_mcontext.arm_r8, context->uc_mcontext.arm_r9, context->uc_mcontext.arm_r10, context->uc_mcontext.arm_fp);
  	ERROR("ip: 0x%x, sp: 0x%x, lr: 0x%x, pc: 0x%x", context->uc_mcontext.arm_ip, context->uc_mcontext.arm_sp, context->uc_mcontext.arm_lr, context->uc_mcontext.arm_pc);

	crashdump::printSymbols((void*)(context->uc_mcontext.arm_pc), (void*)(context->uc_mcontext.arm_lr));

	exit(0);
}
