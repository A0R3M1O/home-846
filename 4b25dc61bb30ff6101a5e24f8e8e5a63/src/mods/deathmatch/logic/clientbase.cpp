/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * @author Aly Cardinal
 * @link https://vk.com/id650525154 
 * @community https://vk.com/a.home846
 * 
*/

#include "stdinc.h"
#include "core/core.h"
#include "multiplayer/multiplayer_init.h"

ARMHook 	*pGTASAHook 	= 0;

void *mainThread(void *p)
{	
	pthread_exit(0);
}

void *uiThread(void *p)
{	
    static bool bHideLayoutCut = true;

    while(true)
    {
        // hide android system ui stuff
        javawrapper::hideNavBar();

        if(bHideLayoutCut) {
            javawrapper::hideLayoutCut();
        }
    }

	pthread_exit(0);
}

void CClient::ProcessCommand(const char* szArguments)
{
    LOG("Client command executed: %s", szArguments);

    if (szArguments && szArguments[0] != '\0')
    {
        if (strcmp(szArguments, "setup") == 0)
        {
            // save storage
	        utilities::setClientStorage(
	        	"/storage/emulated/0/Android/data/com.rockstargames.gtasa/files/"
	        	);

            // init ARM
	        pGTASAHook = new ARMHook(g_GTASAHandle, core::getGame()->FindGameVersion() == VERSION_A_108 ? ARM_TRAMPOLINE_START_18 : ARM_TRAMPOLINE_START_20, ARM_TRAMPOLINE_SIZE); 
	        InitGlobalHooksAndPatches();

            // create main thread
	        pthread_t thread;
	        pthread_create(&thread, 0, mainThread, 0);	

            // create ui thread
	        pthread_t ui_thread;
	        pthread_create(&ui_thread, 0, uiThread, 0);	
        }
        else if (strcmp(szArguments, "unload") == 0)
        {
            javawrapper::uninitialize();

            if(pGTASAHook)
	        {
	        	delete pGTASAHook;
	        	pGTASAHook = 0;
	        }
        }
        else if (strcmp(szArguments, "start") == 0)
        {
            uintptr_t FUNC_StartGameScreen_OnNewGameCheck = utilities::findMethod("libGTASA.so", "_ZN15StartGameScreen14OnNewGameCheckEv");

            // StartGameScreen::OnNewGameCheck
		    (( void (*)())(SA(FUNC_StartGameScreen_OnNewGameCheck)))();
        }
    }
}
