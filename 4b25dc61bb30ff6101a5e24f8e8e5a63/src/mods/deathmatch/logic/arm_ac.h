#ifndef ARM_AC_H
#define ARM_AC_H

long arm_analyze_library_size(const char* szLibName);
bool arm_analyze(uint32_t* dwRetData);
void arm_stop();

#define ARM_LIBCHECK \
	uint32_t dwInjectInfo = 0x00; \
	if(arm_analyze(&dwInjectInfo) == false) \
	{ \
		if(dwInjectInfo != 0x00) { \
			arm_stop(); \
		} \
	} \

#endif // ARM_AC_H