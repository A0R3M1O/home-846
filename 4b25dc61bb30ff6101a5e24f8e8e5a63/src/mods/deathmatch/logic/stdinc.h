#ifndef STDINC_H
#define STDINC_H

#define ARM_TRAMPOLINE_START_18     0x180694
#define ARM_TRAMPOLINE_START_20     0x1A9E0C
#define ARM_TRAMPOLINE_SIZE         0x1A36

#include <jni.h>
#include <dlfcn.h>
#include <link.h>
#include <sys/mman.h>
#include <pthread.h>
#include <cstdlib>
#include <vector>
#include <list>
#include <unistd.h>
#include <algorithm>
#include <chrono>
#include <string.h>
#include <stdio.h>
#include <map>
#include <set>
#include <string>
#include <sstream>
#include <cstdio>
#include <cstring>

extern uintptr_t g_GTASAHandle;
extern uintptr_t g_ClientHandle;
extern uintptr_t g_SCAndHandle;
extern uintptr_t g_ImmEmulatorHandle;

#define SA(dest) (g_GTASAHandle + dest)
#define MTA(dest) (g_ClientHandle + dest)
#define SC(dest) (g_SCAndHandle + dest)
#define IMM(dest) (g_ImmEmulatorHandle + dest)

#include "version.h"
#include "clientbase.h"
#include "arm_ac.h"

#include "game_sa/game_sa.h"

// import utilities
#include "../../../utilities/armhook.h"
#include "../../../utilities/utilities.h"
#include "../../../utilities/log.h"
#include "../../../utilities/crashdump.h"
#include "../../../utilities/javawrapper.h"

#endif // STDINC_H