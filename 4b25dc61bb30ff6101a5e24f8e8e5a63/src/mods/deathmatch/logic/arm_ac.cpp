/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * @author Aly Cardinal
 * @link https://vk.com/id650525154 
 * @community https://vk.com/a.home846
 * 
*/

#include "stdinc.h"
#include <dirent.h>

uint8_t usStopClient = 0;

#define GAME_PACKET_NAME "com.rockstargames.gtasa"

void arm_stop()
{
    usStopClient = 1;
}

long arm_analyze_library_size(const char* szLibName)
{
    char filename[0xFF] = {0},
    buffer[2048] = {0};
    FILE *fp = 0;
    long size = 0;

    sprintf( filename, "/data/data/%s/lib/%s", GAME_PACKET_NAME, szLibName );

    fp = fopen( filename, "rt" );
    if(fp) 
    {
        fseek(fp, 0, SEEK_END);
        size = ftell(fp);

        // LOG("[%s] size: 0x%X", szLibName, size);

        fclose(fp);
    }
    
    return size;
}

// 3/4
bool arm_diagnostics_offsets()
{
    uintptr_t addrFind = 0;

    // if the previous methods were powerless
    // lol

    /**
    addrFind = utilities::findMethod("libGTASA.so", "_ZN9AsiLoader7pathElfEv");
    LOG("addr find : 0x%X", addrFind);
    addrFind = utilities::findMethod("libhmta.so", "_ZN9AsiLoader7pathElfEv");
    LOG("addr find : 0x%X", addrFind);
    addrFind = utilities::findMethod("libbass.so", "_ZN9AsiLoader7pathElfEv");
    LOG("addr find : 0x%X", addrFind);
    addrFind = utilities::findMethod("libSCAnd.so", "_ZN9AsiLoader7pathElfEv");
    LOG("addr find : 0x%X", addrFind);
    addrFind = utilities::findMethod("libImmEmulatorJ.so", "_ZN9AsiLoader7pathElfEv");
    LOG("addr find : 0x%X", addrFind);
    addrFind = utilities::findMethod("libraknet.so", "_ZN9AsiLoader7pathElfEv");
    LOG("addr find : 0x%X", addrFind);
    addrFind = utilities::findMethod("libMTASA_Engine.so", "_ZN9AsiLoader7pathElfEv");
    LOG("addr find : 0x%X", addrFind);
    */
    // no! it's bullshit
    
    return false;
}

// 4/4
bool arm_diagnostics_libs()
{
    // LOG("efw");

    int iUnknownDetects = 0;

    char filename[4096+1] = { 0 };
	sprintf(filename, "/data/data/%s/lib/", GAME_PACKET_NAME);
	
    DIR *dir;
	struct dirent *ent;

	if((dir = opendir(filename)) != NULL) 
	{
		while((ent = readdir(dir)) != NULL) 
		{
			if(ent->d_name)
			{
				if(strstr(ent->d_name, ".so")) 
				{
                    if(strncmp(ent->d_name, "libGTASA.so", 11) == 0) {
                        LOG("lib: %s - OK!", ent->d_name);
                    }
                    else if(strncmp(ent->d_name, "libbass.so", 10) == 0) {
                        LOG("lib: %s - OK!", ent->d_name);
                    }
                    else if(strncmp(ent->d_name, "libSCAnd.so", 11) == 0) {
                        LOG("lib: %s - OK!", ent->d_name);
                    }
                    else if(strncmp(ent->d_name, "libImmEmulatorJ.so", 18) == 0) {
                        LOG("lib: %s - OK!", ent->d_name);
                    }
                    else if(strncmp(ent->d_name, "libhmta.so", 10) == 0) {
                        LOG("lib: %s - OK!", ent->d_name);
                    }
                    else if(strncmp(ent->d_name, "libraknet.so", 12) == 0) {
                        LOG("lib: %s - OK!", ent->d_name);
                    }
                    else if(strncmp(ent->d_name, "libMTASA_Engine.so", 18) == 0) {
                        LOG("lib: %s - OK!", ent->d_name);
                    }
                    else
                    {
                        LOG("lib: %s - ERROR!", ent->d_name);
                        iUnknownDetects++;
                    }
				}
			}
		}

	  	closedir(dir);
	}

    return iUnknownDetects > 0 ? true : false;
}

// 2/4
bool arm_diagnostics_sizes()
{
    // current size
    long gtasaSize = arm_analyze_library_size("libGTASA.so");
    long hmtaSize = arm_analyze_library_size("libhmta.so");
    long bassSize = arm_analyze_library_size("libbass.so");
    long scAndSize = arm_analyze_library_size("libSCAnd.so");
    long immEmuSize = arm_analyze_library_size("libImmEmulatorJ.so");
    long raknetSize = arm_analyze_library_size("libraknet.so");

    // max size
    long gtasaSizeMax   = 0x611874; // [libGTASA.so] size: 0x611874
    long hmtaSizeMax    = 0x576E4; // [libhmta.so] size: 0x576E4
    long bassSizeMax    = 0x37598; // [libbass.so] size: 0x37598
    long scAndSizeMax   = 0xE77371; // [libSCAnd.so] size: 0xE77371
    long immEmuSizeMax  = 0x13F3F8; // [libImmEmulatorJ.so] size: 0x13F3F8
    long raknetSizeMax  = 0x102C5C; // [libraknet.so] size: 0x102C5C

    bool bValidSizes = (
        (gtasaSize == gtasaSizeMax) && 
        (hmtaSize == hmtaSizeMax) && 
        (bassSize == bassSizeMax) && 
        (scAndSize == scAndSizeMax) && 
        (immEmuSize == immEmuSizeMax) &&
        (raknetSize == raknetSizeMax)
        ) ? true : false;

    return !bValidSizes;
}

// 1/4
bool arm_diagnostics_injects()
{
    char filename[0xFF] = {0},
    buffer[2048] = {0};
    FILE *fp = 0;

    sprintf( filename, "/proc/%d/maps", getpid() );

    fp = fopen( filename, "rt" );

    if(fp == 0) {
        return 0;
    }

    int iInjectSearchResult = 0;
    bool bValidLibrary = false;
    bool bValidSizes = false;
    char* szLibs = nullptr;

    while(fgets(buffer, sizeof(buffer), fp)) 
    {
        szLibs = strstr(buffer, "/lib/arm/");

        if(szLibs)
        {
            bValidLibrary = (
                strncmp(szLibs, "/lib/arm/libGTASA.so", 20) == 0 || 
                strncmp(szLibs, "/lib/arm/libbass.so", 19) == 0 || 
                strncmp(szLibs, "/lib/arm/libSCAnd.so", 20) == 0 || 
                strncmp(szLibs, "/lib/arm/libImmEmulatorJ.so", 27) == 0 || 
                strncmp(szLibs, "/lib/arm/libhmta.so", 19) == 0 ||
                strncmp(szLibs, "/lib/arm/libraknet.so", 21) == 0 ||
                strncmp(szLibs, "/lib/arm/libMTASA_Engine.so", 27) == 0 
                ) ? true : false;
            
            if(!bValidLibrary) {
                iInjectSearchResult++;
            }

            szLibs = nullptr;
        }
    }

    fclose(fp);

    return (iInjectSearchResult > 0) ? true : false;
}

bool arm_analyze(uint32_t* dwRetData)
{
    LOG("[ARM-AC] analyze..");

    uint32_t dwResult = 0x00;

    // INJECTS
    if(arm_diagnostics_injects()) 
    {
        LOG("[ARM-AC] Error: #012");
        dwResult = 0xFF;
    }

    // SIZES
    if(arm_diagnostics_sizes()) 
    {
        LOG("[ARM-AC] Error: #013");
        dwResult = 0xFF;
    }

    // LIBS
    if(arm_diagnostics_libs())
    {
        LOG("[ARM-AC] Error: #014");
        dwResult = 0xFF;
    }

    // OFFSETS: todo
    if(arm_diagnostics_offsets()) 
    {
        LOG("[ARM-AC] Error: #015");
        dwResult = 0xFF;
    }

    // final result
    if(dwResult != 0xFF) 
    {
        LOG("[ARM-AC] Analysis completed successfully, no injects detected!");
    }
    else
    {
        LOG("[ARM-AC] Interference detected!");
    }

    *dwRetData = dwResult;
    return (dwResult != 0x00) ? false : true;
}