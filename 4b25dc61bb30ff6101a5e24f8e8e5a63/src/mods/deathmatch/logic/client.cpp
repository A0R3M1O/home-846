/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * @author Aly Cardinal
 * @link https://vk.com/id650525154 
 * @community https://vk.com/a.home846
 * 
*/

#include "stdinc.h"
#include "core/core.h"
#include "multiplayer/multiplayer_init.h"

uintptr_t 	g_GTASAHandle 		= 0;
uintptr_t 	g_ClientHandle 		= 0;
uintptr_t 	g_SCAndHandle 		= 0;
uintptr_t 	g_ImmEmulatorHandle = 0;

extern uint8_t usStopClient;

void initMTASA(JNIEnv* pEnv, jobject thiz)
{
    LOG("Initializing MTA:SA Client..");

	ARM_LIBCHECK
	if(usStopClient == 1) 
	{
		LOG("Install the latest version of multiplayer from official sources to avoid this error.");
		return;
	}

    javawrapper::initialize(pEnv, thiz);
    CClient::ProcessCommand("setup");
}

extern "C"
{
	JNIEXPORT void JNICALL Java_com_nvidia_devtech_NvEventQueueActivity_initMTASA(JNIEnv* pEnv, jobject thiz)
	{
        initMTASA(pEnv, thiz);
    }
}

jint JNI_OnLoad(JavaVM *vm, void *reserved)
{
	LOG("Library loaded!");
	LOG("Version: " CLIENT_VERSION " Time: " __TIME__ " Date: " __DATE__);
    
	javawrapper::setGlobalVM(vm);

	srand(time(0));

    // initialize core
	core::initialize(); 

	// initialize crash handler
	crashdump::initialize();
    
    // load mods
    modmanager::Load();	

	return JNI_VERSION_1_4;
}

void JNI_OnUnload(JavaVM *vm, void *reserved)
{
	g_GTASAHandle = 0;
	g_ClientHandle = 0;
	g_SCAndHandle = 0;
	g_ImmEmulatorHandle = 0;
}

void InitClient()
{
	g_GTASAHandle 		= utilities::getLibraryHandle("libGTASA.so");
	g_ClientHandle 		= utilities::getLibraryHandle("libhmta.so");
	g_SCAndHandle 		= utilities::getLibraryHandle("libSCAnd.so");
	g_ImmEmulatorHandle = utilities::getLibraryHandle("libImmEmulatorJ.so");
}