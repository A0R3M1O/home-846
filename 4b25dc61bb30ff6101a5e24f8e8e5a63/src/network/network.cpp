/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * @author Aly Cardinal
 * @link https://vk.com/id650525154 
 * @community https://vk.com/a.home846
 * 
*/

#include "main.h"
#include "network.h"

char network::g_szHostName[MAX_HOSTNAME_LEN+1];
char network::g_szHost[MAX_HOST_LEN+1];
int  network::g_iPort;
char network::g_szPassword[MAX_PASS_LEN+1];
unsigned char network::g_iState = CONNECTION_STATE_NONE;
unsigned int network::g_dwLastNetworkTick;

void network::Connect(const char* szHost, int iPort, const char* szPassword)
{
    netapi::initialize();

    strcpy(g_szHostName, "Multi Theft Auto: San Andreas");
    strncpy(g_szHost, szHost, sizeof(g_szHost));

    g_iPort = iPort;

    network::g_dwLastNetworkTick = utilities::getTickCount();
    network::g_iState = CONNECTION_STATE_WAIT_CONNECT;
}

void network::Process()
{
    switch(network::g_iState)
    {
        case CONNECTION_STATE_NONE: {
            break;
        }

        case CONNECTION_STATE_WAIT_CONNECT: {
            network::waitConnectProcessing();
            break;
        }

        case CONNECTION_STATE_CONNECTING: 
        {
            packethandler::ProcessPacket();
            break;
        }

        case CONNECTION_STATE_WAIT_JOINING: {
            break;
        }

        case CONNECTION_STATE_CONNECTED: {
            break;
        }

        case CONNECTION_STATE_DISCONNECTED: {
            break;
        }
    }
}

void network::waitConnectProcessing()
{
    if(utilities::getTickCount() - network::g_dwLastNetworkTick < 3000) {
        return;
    }

    LOG("connecting to %s:%d", g_szHost, g_iPort);

    netapi::Connect(g_szHost, g_iPort, 0, 0);
    
    network::g_dwLastNetworkTick = utilities::getTickCount();
    network::g_iState = CONNECTION_STATE_CONNECTING;
}