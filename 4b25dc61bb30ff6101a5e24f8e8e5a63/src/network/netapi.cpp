#include "main.h"
#include "network.h"

unsigned int peer = 0;
uintptr_t g_rakLib = 0;

#define RL(a) (g_rakLib + a)

void netapi::initialize()
{
    g_rakLib = utilities::getLibraryHandle("libraknet.so");
    if(!g_rakLib) 
    {
        ERROR("libraknet.so not found!");
        return;
    }

    return;
}

void netapi::Connect(const char* host, unsigned short remotePort, const char *passwordData, int passwordDataLength, unsigned publicKey, unsigned connectionSocketIndex, unsigned sendConnectionAttemptCount, unsigned timeBetweenSendConnectionAttemptsMS, unsigned timeoutTime)
{
    return;
}

int netapi::receivePacket()
{
    return 0;
}