/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * @author Aly Cardinal
 * @link https://vk.com/id650525154 
 * @community https://vk.com/a.home846
 * 
*/

#include "main.h"
#include "network.h"
#include "packethandler.h"

struct Packet
{
	/// Server only - this is the index into the player array that this playerId maps to
	int playerIndex;

	/// The system that send this packet.
	int playerId;

	/// The length of the data in bytes
	/// \deprecated You should use bitSize.
	unsigned int length;

	/// The length of the data in bits
	unsigned int bitSize;

	/// The data from the sender
	unsigned char* data;

	/// @internal
	/// Indicates whether to delete the data, or to simply delete the packet.
	bool deleteData;
};

unsigned char GetPacketID(Packet* p)
{
	if(p == 0) {
		return 255;
	}

	if ((unsigned char)p->data[0] == 40) {
		return (unsigned char) p->data[sizeof(unsigned char) + sizeof(unsigned long)];
	}

	return (unsigned char) p->data[0];
}

bool packethandler::ProcessPacket()
{
	return false;

	Packet* pkt = 0;

	while(pkt = (Packet*)netapi::receivePacket()) 
	{
		unsigned char id = GetPacketID(pkt);

		LOG("packet received: %d", id);
	}

    return true;
}