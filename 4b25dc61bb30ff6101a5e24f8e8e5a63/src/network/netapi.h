#ifndef NET_API_H
#define NET_API_H

class netapi
{
private:
    /* data */
public:
    static void initialize();
    static void Connect(const char* host, unsigned short remotePort, const char *passwordData, int passwordDataLength, unsigned publicKey = 0, unsigned connectionSocketIndex=0, unsigned sendConnectionAttemptCount=6, unsigned timeBetweenSendConnectionAttemptsMS=1000, unsigned timeoutTime=0);
    static int receivePacket();
};

#endif // NET_API_H