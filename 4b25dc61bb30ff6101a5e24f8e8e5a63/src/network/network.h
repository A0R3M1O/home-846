#ifndef NETWORK_H
#define NETWORK_H

#include "common.h"
#include "Packets.h"
#include "rpc_enums.h"
#include "SyncStructures.h"
#include "netapi.h"
#include "netcommon.h"
#include "packethandler.h"

#define MAX_HOST_LEN        32
#define MAX_PASS_LEN        255
#define MAX_HOSTNAME_LEN    255

#define CONNECTION_STATE_NONE           0
#define CONNECTION_STATE_WAIT_CONNECT   1
#define CONNECTION_STATE_CONNECTING     2
#define CONNECTION_STATE_WAIT_JOINING   3
#define CONNECTION_STATE_CONNECTED      4
#define CONNECTION_STATE_DISCONNECTED   5

class network
{
private:
    static char g_szHostName[MAX_HOSTNAME_LEN+1];
    static char g_szHost[MAX_HOST_LEN+1];
    static int  g_iPort;
    static char g_szPassword[MAX_PASS_LEN+1];
    static unsigned char g_iState;
    static unsigned int g_dwLastNetworkTick;

public:
    static void Connect(const char* szHost, int iPort, const char* szPassword = nullptr);
    static void Process();
    static void waitConnectProcessing();
};


#endif // NETWORK_H