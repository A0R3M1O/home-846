/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * @author Aly Cardinal
 * @link https://vk.com/id650525154 
 * @community https://vk.com/a.home846
 * 
*/

#include "main.h"
#include "game.h"

game::game()
{
    LOG("initializing game interface..");
}

extern char* WORLD_PLAYERS;
uintptr_t game::FindPlayer() {
    return *(uintptr_t*)WORLD_PLAYERS;
}

void game::toggleRadar(bool iToggle)
{
	*(uint8_t*)(SA(0x8EF36B)) = (uint8_t)!iToggle;
}

void game::displayHUD(bool bDisp)
{
	if(bDisp)
	{	
		// CTheScripts11bDisplayHud
		*(uint8_t*)(SA(0x7165E8)) = 1;
		toggleRadar(1);
	} 
	else 
	{
		*(uint8_t*)(SA(0x7165E8)) = 0;
		toggleRadar(0);
	}
}

void game::displayWidgets(bool bDisp)
{
    // CTouchInterface::DrawAll
    WriteMemory(SA(0x0026EE80), bDisp ? (uintptr_t)"\xF8\xB5" : (uintptr_t)"\xF7\x46", 2);
}

void game::getScreenSize(float* x, float* y, float* i)
{
	UnFuck(SA(0x5DDD40));
	UnFuck(SA(0x5DDD44));
	
	*x = *(float*)(SA(0x5DDD40));
	*y = *(float*)(SA(0x5DDD44));	
	*i = 0.0f;
}
