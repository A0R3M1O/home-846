#ifndef GAME_H
#define GAME_H

#include "camera.h"

enum eGameVersion
{
    VERSION_ALL = 0,
    VERSION_A_108 = 108,
    VERSION_A_200 = 200,
    VERSION_UNKNOWN = 0xFF
};

class game
{
private:
    /* data */
public:
    game(/* args */);
    ~game() {};

    eGameVersion FindGameVersion() {   
        return *(uint32_t*)(SA(0x001AE008)) == 0x3F246 ? VERSION_A_108 : VERSION_A_200;
    };

    uintptr_t FindPlayer();
    static void toggleRadar(bool iToggle);
    static void displayHUD(bool bDisp);
    static void displayWidgets(bool bDisp);
    static void getScreenSize(float* x, float* y, float* i);
};


#endif // GAME_H