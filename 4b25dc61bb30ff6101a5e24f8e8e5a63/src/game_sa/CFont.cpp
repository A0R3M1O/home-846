#include "../main.h"
#include "../multiplayer/multiplayer.h"

uint16_t *wszTextDrawGXT= new uint16_t[2000];

void CFont::AsciiToGxtChar(const char *szASCII, uint16_t *wszGXT)
{
	((void (*)(const char *, uint16_t *))(multiplayer::FUNC_CFont_AsciiToGxtChar + 1))(szASCII, wszGXT);
}

void CFont::PrintString(float fX, float fY, char *szText)
{
	uint16_t *gxt_string = new uint16_t[800];
	AsciiToGxtChar(szText, gxt_string);

	(( void (*)(float, float, uint16_t*))(multiplayer::FUNC_CFont_PrintString+1))(fX, fY, gxt_string);
	delete gxt_string;

	(( void (*)())(multiplayer::FUNC_CFont_RenderFontBuffer+1))();
}

void CFont::SetColor(CRGBA color)
{
	((void (*)(CRGBA))(multiplayer::FUNC_CFont_SetColor + 1))(color);
}

void CFont::SetDropColor(CRGBA color)
{
	((void (*)(CRGBA))(multiplayer::FUNC_CFont_SetDropColor + 1))(color);
}

void CFont::SetEdge(bool on)
{
	((void (*)(bool))(multiplayer::FUNC_CFont_SetEdge + 1))(on);
}

void CFont::SetJustify(bool on)
{
	((void (*)(bool))(multiplayer::FUNC_CFont_SetJustify + 1))(on);
}

void CFont::SetScale(float fValue) 
{
	((void (*)(float))(multiplayer::FUNC_CFont_SetScale + 1))(fValue);
}

void CFont::SetScaleXY(float fValueX, float fValueY) 
{
	*(float *)(multiplayer::FUNC_CFont_SetScaleY) = fValueY;
	*(float *)(multiplayer::FUNC_CFont_SetScaleX) = fValueX;
}

void CFont::SetOrientation(unsigned char ucValue) 
{	
	((void (*)(int))(multiplayer::FUNC_CFont_SetOrientation + 1))(ucValue);
}

void CFont::SetFontStyle(unsigned char ucValue) 
{
	((void (*)(int))(multiplayer::FUNC_CFont_SetFontStyle + 1))(ucValue);
}

void CFont::SetProportional(unsigned char ucValue) 
{
	((void (*)(bool))(multiplayer::FUNC_CFont_SetProportional + 1))(ucValue);
}

void CFont::SetRightJustifyWrap(float fValue) 
{	
	((void (*)(float))(multiplayer::FUNC_CFont_SetRightJustifyWrap + 1))(fValue);
}

void CFont::SetBackground(bool enable, bool includeWrap) 
{
	((void (*)(bool, bool))(multiplayer::FUNC_CFont_SetBackground + 1))(enable, includeWrap);
}

void CFont::SetBackgroundColor(CRGBA uiColor) 
{
	((void (*)(CRGBA))(multiplayer::FUNC_CFont_SetBackgroundColor + 1))(uiColor);
}

void CFont::SetWrapx(float fValue) 
{
	((void (*)(float))(multiplayer::FUNC_CFont_SetWrapx + 1))(fValue);
}

void CFont::SetCentreSize(float fValue) 
{
	((void (*)(float))(multiplayer::FUNC_CFont_SetCentreSize + 1))(fValue);
}

void CFont::SetDropShadowPosition(signed char scValue) 
{
	((void (*)(short))(multiplayer::FUNC_CFont_SetDropShadowPosition + 1))(scValue);
}

float CFont::GetHeight(bool unk)
{
	return ((float (*)(bool))(multiplayer::FUNC_CFont_GetHeight + 1))(unk);
}

float CFont::GetStringWidth(uint16_t* str)
{
	return ((float (*)(uint16_t*))(multiplayer::FUNC_CFont_GetStringWidth + 1))(str);
}
void CFont::GetTextRect(CRect rect, float x, float y)
{
	((float (*)(CRect, float, float, uint16_t *))(multiplayer::FUNC_CFont_GetTextRect + 1))(rect, x, y, wszTextDrawGXT);
}