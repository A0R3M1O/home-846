#pragma once

typedef struct
{
	unsigned int   m_nFlags;
    float        m_vecOffset[3];
    class IFrame  *m_pIFrame;
    unsigned int   m_nNodeId;
} AnimBlendFrameData;

class CPed;