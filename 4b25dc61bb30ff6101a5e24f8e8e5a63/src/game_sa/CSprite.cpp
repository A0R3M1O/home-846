#include "../main.h"

void CSprite::RenderOneXLUSprite(float x, float y, float z, float halfWidth, float halfHeight, 
								 unsigned char red, unsigned char green, unsigned char blue, 
								 short alpha, float rhw, unsigned char intensity, unsigned char udir, 
								 unsigned char vdir)
{
    ((void (*)(float, float, float, float, float, unsigned char, unsigned char, unsigned char, short, float, unsigned char, unsigned char, unsigned char))
    (SA(utilities::findMethod("libGTASA.so", "_ZN7CSprite18RenderOneXLUSpriteEfffffhhhsfhhhff"))))
    (x, y, z, halfWidth, halfHeight, red, green, blue, alpha, rhw, intensity, udir, vdir);
}

bool CSprite::CalcScreenCoors(RwV3d const &posn, RwV3d *out, float *w, float *h, bool checkMaxVisible, bool checkMinVisible)
{
    return ((bool (*)(RwV3d const&, RwV3d *, float *, float *, bool, bool))(SA(utilities::findMethod("libGTASA.so", "_ZN7CSprite15CalcScreenCoorsERK5RwV3dPS0_PfS4_bb"))))(posn, out, w, h, checkMaxVisible, checkMinVisible);
}