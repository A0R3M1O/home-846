#pragma once

#include "RW/RenderWare.h"
#include "scripting.h"

#include "CFont.h"
#include "CText.h"
#include "CPed.h"
#include "CSprite.h"
#include "C2dEffect.h"
#include "CStreaming.h"
#include "CWorld.h"

#include "CVector.h"
#include "CVector2D.h"
#include "CRect.h"
#include "CRGBA.h"

#include "ePedState.h"
#include "eAnimations.h"
#include "eAudioEvents.h"
#include "eGangID.h"
#include "eModelID.h"
#include "ePedAnims.h"
#include "ePedBones.h"
#include "ePedType.h"
#include "eWeaponFire.h"
#include "eWeaponType.h"