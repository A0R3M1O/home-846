/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * @author Aly Cardinal
 * @link https://vk.com/id650525154 
 * @community https://vk.com/a.home846
 * 
*/

#include "main.h"
#include "multiplayer.h"

 char* (*RenderQueue_ProcessCommand)(uintptr_t thiz, char* a2);
 char* HOOK_RenderQueue_ProcessCommand(uintptr_t thiz, char* a2);

 RwFrame* (*CClumpModelInfo_GetFrameFromId)(RpClump*, int);
 RwFrame* HOOK_CClumpModelInfo_GetFrameFromId(RpClump* a1, int a2);

//////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////
//
// RenderQueue::ProcessCommand
//
//////////////////////////////////////////////////////////////////////////////////////////
char* HOOK_RenderQueue_ProcessCommand(uintptr_t thiz, char* a2)
{
	if (thiz && a2) {
		return RenderQueue_ProcessCommand(thiz, a2);
	}

	return nullptr;
}

RwFrame* CClumpModelInfo_GetFrameFromId_Post(RwFrame* pFrameResult, RpClump* pClump, int id)
{
	if (pFrameResult)
		return pFrameResult;

	uintptr_t calledFrom = 0;
	__asm__ volatile ("mov %0, lr" : "=r" (calledFrom));
	calledFrom -= g_GTASAHandle;

	// Don't check frame if call can legitimately return NULL
	if (calledFrom == 0x00515708                // CVehicle::SetWindowOpenFlag
		|| calledFrom == 0x00515730             // CVehicle::ClearWindowOpenFlag
		|| calledFrom == 0x00338698             // CVehicleModelInfo::GetOriginalCompPosition
		|| calledFrom == 0x00338B2C)            // CVehicleModelInfo::CreateInstance
		return NULL;

	for (uint i = 2; i < 40; i++)
	{
		RwFrame* pNewFrameResult = NULL;
		uint     uiNewId = id + (i / 2) * ((i & 1) ? -1 : 1);
		
		pNewFrameResult = ((RwFrame * (*)(RpClump * pClump, int id))(SA(0x00335CC0 + 1)))(pClump, i);

		if (pNewFrameResult)
		{
			return pNewFrameResult;
		}
	}

	return NULL;
}

//////////////////////////////////////////////////////////////////////////////////////////
//
// CClumpModelInfo::GetFrameFromId
//
//////////////////////////////////////////////////////////////////////////////////////////
RwFrame* HOOK_CClumpModelInfo_GetFrameFromId(RpClump* a1, int a2)
{
	return CClumpModelInfo_GetFrameFromId_Post(CClumpModelInfo_GetFrameFromId(a1, a2), a1, a2);
}

//////////////////////////////////////////////////////////////////////////////////////////
//
// GetTexture
//
//////////////////////////////////////////////////////////////////////////////////////////
uintptr_t(*GetTexture)(const char*);
uintptr_t HOOK_GetTexture(const char* a1)
{
	uintptr_t tex = ((uintptr_t(*)(const char*))(SA(0x001BE990 + 1)))(a1);

	if (!tex)
	{
		LOG("Texture %s was not found", a1);
		return 0;
	}

	++*(uintptr_t*)(tex + 84);
	return tex;
}

static uint32_t dwRLEDecompressSourceSize = 0;

//////////////////////////////////////////////////////////////////////////////////////////
//
// OS_FileRead
//
//////////////////////////////////////////////////////////////////////////////////////////

int (*OS_FileRead)(void* a1, void* a2, int a3);
int HOOK_OS_FileRead(void* a1, void* a2, int a3)
{
	uintptr_t calledFrom = 0;
	__asm__ volatile ("mov %0, lr" : "=r" (calledFrom));
	calledFrom -= g_GTASAHandle;

	if (!a3) {
		return 0;
	}

	if (calledFrom == 0x1BDD34 + 1)
	{
		int retn = OS_FileRead(a1, a2, a3);
		dwRLEDecompressSourceSize = *(uint32_t*)a2; // save value for RLEDecompress
		return retn;
	}

	return OS_FileRead(a1, a2, a3);
}

//////////////////////////////////////////////////////////////////////////////////////////
//
// RLEDecompress
//
//////////////////////////////////////////////////////////////////////////////////////////
uint8_t* (*RLEDecompress)(uint8_t* pDest, size_t uiDestSize, uint8_t const* pSrc, size_t uiSegSize, uint32_t uiEscape);
uint8_t* HOOK_RLEDecompress(uint8_t* pDest, size_t uiDestSize, uint8_t const* pSrc, size_t uiSegSize, uint32_t uiEscape) 
{
	if (!pDest) {
		return pDest;
	}

	if (!pSrc) {
		return pDest;
	}

	uint8_t* pTempDest = pDest;
	const uint8_t* pTempSrc = pSrc;
	uint8_t* pEndOfDest = &pDest[uiDestSize];

	uint8_t* pEndOfSrc = (uint8_t*)&pSrc[dwRLEDecompressSourceSize];

	if (pDest < pEndOfDest) 
	{
		do 
		{
			if (*pTempSrc == uiEscape) 
			{
				uint8_t ucCurSeg = pTempSrc[1];
				if (ucCurSeg) 
				{
					uint8_t* ucCurDest = pTempDest;
					uint8_t ucCount = 0;
					do 
					{
						++ucCount;

						pDest = (uint8_t*)memcpy(ucCurDest, pTempSrc + 2, uiSegSize);
						
						ucCurDest += uiSegSize;
					} while (ucCurSeg != ucCount);


					pTempDest += uiSegSize * ucCurSeg;
				}
				pTempSrc += 2 + uiSegSize;
			}

			else 
			{
				if (pTempSrc + uiSegSize >= pEndOfSrc)
				{
					return pDest;
				}
				else
				{
					pDest = (uint8_t*)memcpy(pTempDest, pTempSrc, uiSegSize);
					pTempDest += uiSegSize;
					pTempSrc += uiSegSize;
				}
			}
		} while (pEndOfDest > pTempDest);
	}

	dwRLEDecompressSourceSize = 0;

	return pDest;
}

//////////////////////////////////////////////////////////////////////////////////////////
//
// _rwFreeListFreeReal
//
//////////////////////////////////////////////////////////////////////////////////////////
int (*_rwFreeListFreeReal)(int a1, unsigned int a2);
int HOOK_rwFreeListFreeReal(int a1, unsigned int a2)
{
	if (a1) {
		return _rwFreeListFreeReal(a1, a2);
	}

	return 0;
}

//////////////////////////////////////////////////////////////////////////////////////////
//
// CDarkel::RegisterCarBlownUpByPlayer
//
//////////////////////////////////////////////////////////////////////////////////////////
void (*CDarkel__RegisterCarBlownUpByPlayer)(void* pVehicle, int arg2);
void HOOK_CDarkel__RegisterCarBlownUpByPlayer(void* pVehicle, int arg2)
{
	return;
}

//////////////////////////////////////////////////////////////////////////////////////////
//
// CDarkel::ResetModelsKilledByPlayer
//
//////////////////////////////////////////////////////////////////////////////////////////
void (*CDarkel__ResetModelsKilledByPlayer)(int playerid);
void HOOK_CDarkel__ResetModelsKilledByPlayer(int playerid)
{
	return;
}

//////////////////////////////////////////////////////////////////////////////////////////
//
// CDarkel::QueryModelsKilledByPlayer
//
//////////////////////////////////////////////////////////////////////////////////////////
int(*CDarkel__QueryModelsKilledByPlayer)(int, int);
int HOOK_CDarkel__QueryModelsKilledByPlayer(int player, int modelid)
{
	return 0;
}

//////////////////////////////////////////////////////////////////////////////////////////
//
// CDarkel::FindTotalPedsKilledByPlayer
//
//////////////////////////////////////////////////////////////////////////////////////////
int (*CDarkel__FindTotalPedsKilledByPlayer)(int player);
int HOOK_CDarkel__FindTotalPedsKilledByPlayer(int player)
{
	return 0;
}

//////////////////////////////////////////////////////////////////////////////////////////
//
// CDarkel::RegisterKillByPlayer
//
//////////////////////////////////////////////////////////////////////////////////////////
void (*CDarkel__RegisterKillByPlayer)(void* pKilledPed, unsigned int damageWeaponID, bool bHeadShotted, int arg4);
void HOOK_CDarkel__RegisterKillByPlayer(void* pKilledPed, unsigned int damageWeaponID, bool bHeadShotted, int arg4)
{
	return;
}

//////////////////////////////////////////////////////////////////////////////////////////
//
// CRopes::Update
//
//////////////////////////////////////////////////////////////////////////////////////////
void (*CRopes__Update)();
void HOOK_CRopes__Update()
{
	
}

//////////////////////////////////////////////////////////////////////////////////////////
//
// FxEmitterBP_c::Render
//
//////////////////////////////////////////////////////////////////////////////////////////
void (*FxEmitterBP_c__Render)(uintptr_t* a1, int a2, int a3, float a4, char a5);
void HOOK_FxEmitterBP_c__Render(uintptr_t* a1, int a2, int a3, float a4, char a5)
{
	uintptr_t* temp = *((uintptr_t**)a1 + 3);
	if (!temp) {
		return;
	}

	FxEmitterBP_c__Render(a1, a2, a3, a4, a5);
}

//////////////////////////////////////////////////////////////////////////////////////////
//
// CPlayerInfo::FindObjectToSteal
//
//////////////////////////////////////////////////////////////////////////////////////////
uintptr_t (*CPlayerInfo__FindObjectToSteal)(uintptr_t, uintptr_t);
uintptr_t HOOK_CPlayerInfo__FindObjectToSteal(uintptr_t a1, uintptr_t a2)
{
	return 0;
}

//////////////////////////////////////////////////////////////////////////////////////////
//
// RwFrameAddChild
//
//////////////////////////////////////////////////////////////////////////////////////////
int (*RwFrameAddChild)(int, int);
int HOOK_RwFrameAddChild(int a1, int a2)
{
	if (a2 && a1) {
		return RwFrameAddChild(a1, a2);
	}
	
	return 0;
}

//////////////////////////////////////////////////////////////////////////////////////////
//
// CAnimManager::UncompressAnimation
//
//////////////////////////////////////////////////////////////////////////////////////////
void (*CAnimManager__UncompressAnimation)(int, int);
void HOOK_CAnimManager__UncompressAnimation(int a1, int a2)
{
	if (a1) {
		CAnimManager__UncompressAnimation(a1, a2);
	}
}

//////////////////////////////////////////////////////////////////////////////////////////
//
// CCustomRoadsignMgr::RenderRoadsignAtomic
//
//////////////////////////////////////////////////////////////////////////////////////////
void (*CCustomRoadsignMgr__RenderRoadsignAtomic)(int, int);
void HOOK_CCustomRoadsignMgr__RenderRoadsignAtomic(int a1, int a2)
{
	if (a1) {
		CCustomRoadsignMgr__RenderRoadsignAtomic(a1, a2);
	}
}

//////////////////////////////////////////////////////////////////////////////////////////
//
// CUpsideDownCarCheck::IsCarUpsideDown
//
//////////////////////////////////////////////////////////////////////////////////////////
int (*CUpsideDownCarCheck__IsCarUpsideDown)(int, int);
int HOOK_CUpsideDownCarCheck__IsCarUpsideDown(int a1, int a2)
{
	if (*(uintptr_t*)(a2 + 20)) {
		return CUpsideDownCarCheck__IsCarUpsideDown(a1, a2);
	}

	return 0;
}

//////////////////////////////////////////////////////////////////////////////////////////
//
// CAnimBlendNode::FindKeyFrame
//
//////////////////////////////////////////////////////////////////////////////////////////
int (*CAnimBlendNode__FindKeyFrame)(int, float, int, int);
int HOOK_CAnimBlendNode__FindKeyFrame(int a1, float a2, int a3, int a4)
{
	if (*(uintptr_t*)(a1 + 16)) {
		return CAnimBlendNode__FindKeyFrame(a1, a2, a3, a4);
	}

	return 0;
}

////////////////////////////////////////////////////////////////////////
// check a1
uint32_t (*CAEMP3BankLoader__IsSoundBankLoaded)(int a1, int a2, int a3);
uint32_t HOOK_CAEMP3BankLoader__IsSoundBankLoaded(int a1, int a2, int a3)
{
	if(!a1) {
		return 0;
	}
	
	return CAEMP3BankLoader__IsSoundBankLoaded(a1, a2, a3);
}

////////////////////////////////////////////////////////////////////////
// disable garages stuff
void (*CObject__ProcessGarageDoorBehaviour)(int a1, int a2);
void HOOK_CObject__ProcessGarageDoorBehaviour(int a1, int a2)
{
	return;
}

////////////////////////////////////////////////////////////////////////
// Handle CTaskComplexDieInCar::ControlSubTask ped with no vehicle
int (*CTaskComplexDieInCar__ControlSubTask)(int a1, uintptr_t pPed);
int HOOK_CTaskComplexDieInCar__ControlSubTask(int a1, uintptr_t pPed)
{
	if(!a1) {
		return 0;
	}

	if(!*(uintptr_t*)(pPed + 1420)) { // check pPed->pVehicle
		return 0;
	}	

	return CTaskComplexDieInCar__ControlSubTask(a1, pPed);
}

////////////////////////////////////////////////////////////////////////
// frame check null-pointer
int (*RwFrameCloneHierarchy)(int frame);
int HOOK_RwFrameCloneHierarchy(int frame)
{
	return frame ? RwFrameCloneHierarchy(frame) : 0;
}

///////////////////////////////////////////////////////////////////////
// colModel check null-pointer
uint32_t (*CCollision__ProcessVerticalLine)(float *colLine, float *transform, int colModel, int colPoint, int *maxTouchDistance, char seeThrough, int shootThrough, int storedCollPoly);
uint32_t HOOK_CCollision__ProcessVerticalLine(float *colLine, float *transform, int colModel, int colPoint, int *maxTouchDistance, char seeThrough, int shootThrough, int storedCollPoly)
{
	if(colModel) {
		return CCollision__ProcessVerticalLine(colLine, transform, colModel, colPoint, maxTouchDistance, seeThrough, shootThrough, storedCollPoly);
	}

	return 0;
}

///////////////////////////////////////////////////////////////////////
// CPed::PlayFootSteps
void (*CPed__PlayFootSteps)(uintptr_t pPed, int a2, int a3, int a4);
void HOOK_CPed__PlayFootSteps(uintptr_t pPed, int a2, int a3, int a4)
{
	if(!pPed) {
		return;
	}

	// RpAnimBlendClumpGetFirstAssociation
	uintptr_t pClump = *(uintptr_t*)(pPed + 0x18);

	if(pClump) 
	{
		uintptr_t pClumpAssociation = ((uintptr_t (*)(uintptr_t, int, int, int))(SA(0x0033D970 + 1)))(pClump, a2, a3, a4); // TODO: OFFSETS

		if(pClumpAssociation) {
			CPed__PlayFootSteps(pPed, a2, a3, a4);
		}
	}
}

#pragma pack(1)
typedef struct _RES_ENTRY_OBJ
{
	uint8_t a[48]; 			// 0-48
	uintptr_t validate; 	// 48-52
	uint8_t b[4]; 			// 52-56
} RES_ENTRY_OBJ;
static_assert(sizeof(_RES_ENTRY_OBJ) == 56);

///////////////////////////////////////////////////////////////////////
// CCustomBuildingDNPipeline::CustomPipeRenderCB
int (*CCustomBuildingDNPipeline__CustomPipeRenderCB)(uintptr_t resEntry, uintptr_t object, uint8_t type, uint32_t flags);
int HOOK_CCustomBuildingDNPipeline__CustomPipeRenderCB(uintptr_t resEntry, uintptr_t object, uint8_t type, uint32_t flags)
{
	if(!resEntry) {
		return 0;
	}

    uint16_t size = *(uint16_t *)(resEntry + 26);

    if(size)
    {
        RES_ENTRY_OBJ* arr = (RES_ENTRY_OBJ*)(resEntry + 28);

		if(!arr) {
			return 0;
		}

        uint32_t validFlag = flags & 0x84;

        for(int i = 0; i < size; i++)
        {
            if(!arr[i].validate) {
				break;
			}

            if(validFlag)
            {
                uintptr_t* v4 = *(uintptr_t **)(arr[i].validate);

				if(v4)
				{
					if((uintptr_t)v4 > (uintptr_t)0xFFFFFF00) 
					{
						return 0;
					}
                	else
					{	
                    	if(!*(uintptr_t **)v4) {
							return 0;
						}
					}
				}
                
            }
        }
    }

    return CCustomBuildingDNPipeline__CustomPipeRenderCB(resEntry, object, type, flags);
}

///////////////////////////////////////////////////////////////////////
// _rxOpenGLDefaultAllInOneRenderCB
int (*rxOpenGLDefaultAllInOneRenderCB)(uintptr_t resEntry, uintptr_t object, uint8_t type, uint32_t flags);
int HOOK_rxOpenGLDefaultAllInOneRenderCB(uintptr_t resEntry, uintptr_t object, uint8_t type, uint32_t flags)
{
    if(!resEntry) {
		return 0;
	}

    uint16_t size = *(uint16_t *)(resEntry + 26);

    if(size)
    {
        RES_ENTRY_OBJ* arr = (RES_ENTRY_OBJ*)(resEntry + 28);

		if(!arr) {
			return 0;
		}

        uint32_t validFlag = flags & 0x84;

        for(int i = 0; i < size; i++)
        {
            if(!arr[i].validate) {
				break;
			}

            if(validFlag)
            {
                uintptr_t* v4 = *(uintptr_t **)(arr[i].validate);

				if(v4)
				{
					if((uintptr_t)v4 > (uintptr_t)0xFFFFFF00) 
					{
						return 0;
					}
                	else
					{	
                    	if(!*(uintptr_t **)v4) {
							return 0;
						}
					}
				}
                
            }
        }
    }

    return rxOpenGLDefaultAllInOneRenderCB(resEntry, object, type, flags);
}

///////////////////////////////////////////////////////////////////////
// CPed::SetPedPositionInCar
void (*CPed__SetPedPositionInCar)(uintptr_t pPed);
void HOOK_CPed__SetPedPositionInCar(uintptr_t pPed)
{
	if(pPed) {
		CPed__SetPedPositionInCar(pPed);
	}
}

///////////////////////////////////////////////////////////////////////
// CAESoundManager::Service
void (*CAESoundManager__Service)(uintptr_t thiz);
void HOOK_CAESoundManager__Service(uintptr_t thiz)
{
	if(thiz) {
		CAESoundManager__Service(thiz);
	}
}

///////////////////////////////////////////////////////////////////////
// CAutomobile::ProcessControl
void (*CAutomobile__ProcessControl)(uintptr_t thiz);
void HOOK_CAutomobile__ProcessControl(uintptr_t thiz)
{
	// CEntity::GetColModel
	uintptr_t colModel = ((uintptr_t (*)(uintptr_t))(SA(0x003938D4 + 1)))(thiz); // TODO: OFFSETS

	if(colModel) 
	{ 
		uint32_t colData = *(uint32_t *)(colModel + 22);

		if(colData) {
			CAutomobile__ProcessControl(thiz);
		}
	}

	// todo: multiplayer stuff
}

//////////////////////////////////////////////////////////////////////////////////////////
//
// Setup hooks for CrashFixHacks
// TODO: offsets
//////////////////////////////////////////////////////////////////////////////////////////
void multiplayer::InitHooks_CrashFixHacks() 
{
	HookInstall(multiplayer::HOOKPOS_RenderQueue_ProcessCommand, (uintptr_t)HOOK_RenderQueue_ProcessCommand, (uintptr_t*)&RenderQueue_ProcessCommand);

    HookInstall(SA(0x0023BEDC), (uintptr_t)HOOK_OS_FileRead, (uintptr_t*)&OS_FileRead); 
    HookInstall(SA(0x00335CC0), (uintptr_t)HOOK_CClumpModelInfo_GetFrameFromId, (uintptr_t*)&CClumpModelInfo_GetFrameFromId);
	HookInstall(SA(0x00258910), (uintptr_t)HOOK_GetTexture, (uintptr_t*)&GetTexture); 
	HookInstall(SA(0x001BC314), (uintptr_t)HOOK_RLEDecompress, (uintptr_t*)&RLEDecompress);
    HookInstall(SA(0x001B9D74), (uintptr_t)HOOK_rwFreeListFreeReal, (uintptr_t*)&_rwFreeListFreeReal);
    HookInstall(SA(0x002C0304), (uintptr_t)HOOK_CDarkel__RegisterCarBlownUpByPlayer, (uintptr_t*)&CDarkel__RegisterCarBlownUpByPlayer);
	HookInstall(SA(0x002C072C), (uintptr_t)HOOK_CDarkel__ResetModelsKilledByPlayer, (uintptr_t*)&CDarkel__ResetModelsKilledByPlayer);
	HookInstall(SA(0x002C0758), (uintptr_t)HOOK_CDarkel__QueryModelsKilledByPlayer, (uintptr_t*)&CDarkel__QueryModelsKilledByPlayer);
	HookInstall(SA(0x002C0778), (uintptr_t)HOOK_CDarkel__FindTotalPedsKilledByPlayer, (uintptr_t*)&CDarkel__FindTotalPedsKilledByPlayer);
	HookInstall(SA(0x002C0D04), (uintptr_t)HOOK_CDarkel__RegisterKillByPlayer, (uintptr_t*)&CDarkel__RegisterKillByPlayer);
    HookInstall(SA(0x003B67F8), (uintptr_t)HOOK_CRopes__Update, (uintptr_t*)&CRopes__Update);
	HookInstall(SA(0x003AC114), (uintptr_t)HOOK_CPlayerInfo__FindObjectToSteal, (uintptr_t*)&CPlayerInfo__FindObjectToSteal);
    HookInstall(SA(0x0033DA5C), (uintptr_t)HOOK_CAnimManager__UncompressAnimation, (uintptr_t*)&CAnimManager__UncompressAnimation);
	HookInstall(SA(0x00531118), (uintptr_t)HOOK_CCustomRoadsignMgr__RenderRoadsignAtomic, (uintptr_t*)&CCustomRoadsignMgr__RenderRoadsignAtomic);
	HookInstall(SA(0x001AECC0), (uintptr_t)HOOK_RwFrameAddChild, (uintptr_t*)&RwFrameAddChild);
	HookInstall(SA(0x002DFD30), (uintptr_t)HOOK_CUpsideDownCarCheck__IsCarUpsideDown, (uintptr_t*)&CUpsideDownCarCheck__IsCarUpsideDown);
	HookInstall(SA(0x0033AD78), (uintptr_t)HOOK_CAnimBlendNode__FindKeyFrame, (uintptr_t*)&CAnimBlendNode__FindKeyFrame);
    HookInstall(SA(0x0031B164), (uintptr_t)HOOK_FxEmitterBP_c__Render, (uintptr_t*)&FxEmitterBP_c__Render);
	HookInstall(SA(0x0029947C), (uintptr_t)HOOK_CCollision__ProcessVerticalLine, (uintptr_t*)&CCollision__ProcessVerticalLine);
	HookInstall(SA(0x005E57F0), (uintptr_t)HOOK_CPed__PlayFootSteps, (uintptr_t*)&CPed__PlayFootSteps);
	HookInstall(SA(0x004E314C), (uintptr_t)HOOK_CAutomobile__ProcessControl, (uintptr_t*)&CAutomobile__ProcessControl);
	HookInstall(SA(0x0028AAAC), (uintptr_t)HOOK_CCustomBuildingDNPipeline__CustomPipeRenderCB, (uintptr_t*)&CCustomBuildingDNPipeline__CustomPipeRenderCB);
	HookInstall(SA(0x001EEC90), (uintptr_t)HOOK_rxOpenGLDefaultAllInOneRenderCB, (uintptr_t*)&rxOpenGLDefaultAllInOneRenderCB);
	HookInstall(SA(0x002CE788), (uintptr_t)HOOK_CObject__ProcessGarageDoorBehaviour, (uintptr_t*)&CObject__ProcessGarageDoorBehaviour);

	// #define TEST_HOOKS_USING
	#ifdef TEST_HOOKS_USING
	// check
	HookInstall(SA(0x00246984), (uintptr_t)HOOK_CAEMP3BankLoader__IsSoundBankLoaded, (uintptr_t*)&CAEMP3BankLoader__IsSoundBankLoaded);
	HookInstall(SA(0x0047A8B4), (uintptr_t)HOOK_CTaskComplexDieInCar__ControlSubTask, (uintptr_t*)&CTaskComplexDieInCar__ControlSubTask);
	HookInstall(SA(0x001AEB98), (uintptr_t)HOOK_RwFrameCloneHierarchy, (uintptr_t*)&RwFrameCloneHierarchy);
	HookInstall(SA(0x005DF910), (uintptr_t)HOOK_CPed__SetPedPositionInCar, (uintptr_t*)&CPed__SetPedPositionInCar);
	HookInstall(SA(0x00356900), (uintptr_t)HOOK_CAESoundManager__Service, (uintptr_t*)&CAESoundManager__Service);
	#endif
}