/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * @author Aly Cardinal
 * @link https://vk.com/id650525154 
 * @community https://vk.com/a.home846
 * 
*/

#include "main.h"
#include "multiplayer.h"
#include "offsets.h"

void offsets::initialize_1_08()
{
    LOG("initializing offsets..");

    multiplayer::HOOKPOS_CHud_Draw =                                SA(0x003D6E6C);
    multiplayer::HOOKPOS_RenderQueue_ProcessCommand =               SA(0x001A8530);
    multiplayer::FUNC_CFont_AsciiToGxtChar =                        SA(0x00532D00);
    multiplayer::FUNC_CFont_PrintString =                           SA(0x005353B4);
    multiplayer::FUNC_CFont_SetColor =                              SA(0x005336F4);
    multiplayer::FUNC_CFont_SetDropColor =                          SA(0x0053387C);
    multiplayer::FUNC_CFont_SetEdge =                               SA(0x0053394C);
    multiplayer::FUNC_CFont_SetJustify =                            SA(0x005339D0);
    multiplayer::FUNC_CFont_SetScale =                              SA(0x00533694);
    multiplayer::FUNC_CFont_SetScaleY =                             SA(0x0099D754);
    multiplayer::FUNC_CFont_SetScaleX =                             SA(0x0099D750);
    multiplayer::FUNC_CFont_SetOrientation =                        SA(0x005339E8);
    multiplayer::FUNC_CFont_SetFontStyle =                          SA(0x00533748);
    multiplayer::FUNC_CFont_SetProportional =                       SA(0x00533970);
    multiplayer::FUNC_CFont_SetRightJustifyWrap =                   SA(0x0053384C);
    multiplayer::FUNC_CFont_SetBackground =                         SA(0x00533988);
    multiplayer::FUNC_CFont_SetBackgroundColor =                    SA(0x005339A4);
    multiplayer::FUNC_CFont_SetWrapx =                              SA(0x0053381C);
    multiplayer::FUNC_CFont_SetCentreSize =                         SA(0x00533834);
    multiplayer::FUNC_CFont_SetDropShadowPosition =                 SA(0x005338DC);
    multiplayer::FUNC_CFont_GetHeight =                             SA(0x005330F0);
    multiplayer::FUNC_CFont_GetStringWidth =                        SA(0x00534BAC);
    multiplayer::FUNC_CFont_GetTextRect =                           SA(0x005352DC);
    multiplayer::FUNC_CFont_RenderFontBuffer =                      SA(0x0053411C);
    multiplayer::VAR_RS_GLOBAL                      =               SA(0x0095B068);
    multiplayer::FUNC_RwCameraBeginUpdate 			=               SA(0x001AD6C8);
    multiplayer::FUNC_RwCameraEndUpdate 				=           SA(0x001AD6B8);
    multiplayer::FUNC_RwCameraShowRaster				=           SA(0x001AD8C4);
    multiplayer::FUNC_RwRasterCreate 				=               SA(0x001B0778);
    multiplayer::FUNC_RwRasterDestroy 				=               SA(0x001B059C);
    multiplayer::FUNC_RwRasterGetOffset 				=           SA(0x001B0460);
    multiplayer::FUNC_RwRasterGetNumLevels 			=               SA(0x001B06B4);
    multiplayer::FUNC_RwRasterSubRaster 				=           SA(0x001B0724);
    multiplayer::FUNC_RwRasterRenderFast				=           SA(0x001B0500);
    multiplayer::FUNC_RwRasterRender					=           SA(0x001B054C);
    multiplayer::FUNC_RwRasterRenderScaled			=               SA(0x001B0440);
    multiplayer::FUNC_RwRasterPushContext			=               SA(0x001B05E4);
    multiplayer::FUNC_RwRasterPopContext				=           SA(0x001B0674);
    multiplayer::FUNC_RwRasterGetCurrentContext		=               SA(0x001B0414);
    multiplayer::FUNC_RwRasterClear					=               SA(0x001B0498);
    multiplayer::FUNC_RwRasterClearRect				=               SA(0x001B052C);
    multiplayer::FUNC_RwRasterShowRaster				=           SA(0x001B06F0);
    multiplayer::FUNC_RwRasterLock					=               SA(0x001B0814);
    multiplayer::FUNC_RwRasterUnlock					=           SA(0x001B0474);
    multiplayer::FUNC_RwRasterLockPalette			=               SA(0x001B0648);
    multiplayer::FUNC_RwRasterUnlockPalette			=               SA(0x001B0578);
    multiplayer::FUNC_RwImageCreate					=               SA(0x001AF338);
    multiplayer::FUNC_RwImageDestroy					=           SA(0x001AF44C);
    multiplayer::FUNC_RwImageAllocatePixels			=               SA(0x001AF38C);
    multiplayer::FUNC_RwImageFreePixels				=               SA(0x001AF420);
    multiplayer::FUNC_RwImageCopy					=               SA(0x001AFA50);
    multiplayer::FUNC_RwImageResize					=               SA(0x001AF490);
    multiplayer::FUNC_RwImageApplyMask				=               SA(0x001AFBB0);
    multiplayer::FUNC_RwImageMakeMask				=               SA(0x001AF5CC);
    multiplayer::FUNC_RwImageReadMaskedImage			=           SA(0x001AFCF8);
    multiplayer::FUNC_RwImageRead					=               SA(0x001AF74C);
    multiplayer::FUNC_RwImageWrite					=               SA(0x001AF980);
    multiplayer::FUNC_RwImageSetFromRaster			=               SA(0x001B023C);
    multiplayer::FUNC_RwRasterSetFromImage			=               SA(0x001B0260);
    multiplayer::FUNC_RwRasterRead					=               SA(0x001B035C);
    multiplayer::FUNC_RwRasterReadMaskedRaster		=               SA(0x001B03CC);
    multiplayer::FUNC_RwImageFindRasterFormat		=               SA(0x001B0284);
    multiplayer::FUNC_RwTextureCreate				=               SA(0x001B1B4C);
    multiplayer::FUNC__rwStreamInitialize			=               SA(0x001BA93C);
    multiplayer::FUNC_RwStreamOpen					=               SA(0x001BADA4);
    multiplayer::FUNC_RwStreamClose					=               SA(0x001BACF4);
    multiplayer::FUNC_RwStreamRead					=               SA(0x001BAA50);
    multiplayer::FUNC_RwStreamWrite					=               SA(0x001BAB28);
    multiplayer::FUNC_RwStreamSkip					=               SA(0x001BAC70);
    multiplayer::FUNC_RwIm2DGetNearScreenZ			=               SA(0x001B8038);
    multiplayer::FUNC_RwIm2DGetFarScreenZ			=               SA(0x001B8054);
    multiplayer::FUNC_RwRenderStateGet				=               SA(0x001B80A8);
    multiplayer::FUNC_RwRenderStateSet				=               SA(0x001B8070);
    multiplayer::FUNC_RwIm2DRenderLine				=               SA(0x001B80C4);
    multiplayer::FUNC_RwIm2DRenderTriangle			=               SA(0x001B80E0);
    multiplayer::FUNC_RwIm2DRenderPrimitive			=               SA(0x001B80FC);
    multiplayer::FUNC_RwIm2DRenderIndexedPrimitive	=               SA(0x001B8118);
    multiplayer::FUNC_RtPNGImageWrite				=               SA(0x001D6CEC);
    multiplayer::FUNC_RtPNGImageRead					=           SA(0x001D6F84);
    multiplayer::FUNC_ExecuteScriptBuf              =               SA(0x002E1D2C);
    multiplayer::FUNC_TextureDatabaseRuntime_GetTexture =           SA(0x001BE990);
    multiplayer::FUNC_OperatorNew =                                 SA(0x00179B40);
    multiplayer::VAR_WORLD_PLAYERS =                                SA(0x005D021C);
    multiplayer::ADDR_SHADOWCRASH1 =                                SA(0x0039B2C0);
    multiplayer::ADDR_SHADOWCRASH2 =                                SA(0x003A019C);
    multiplayer::ADDR_SHADOWCRASH3 =                                SA(0x0039B2C4);
    multiplayer::ADDR_COLLISION_POOL1 =                             SA(0x0029554A);
    multiplayer::ADDR_COLLISION_POOL2 =                             SA(0x00295556);
    multiplayer::HOOKPOS_RwCameraSetNearClipPlane =                 SA(0x001AD6F4);
    multiplayer::HOOKPOS_RwCameraSetFarClipPlane =                  SA(0x001AD710);
}

void offsets::initialize_2_00()
{
    LOG("initializing offsets..");
    // 2.00 TODO ~
}