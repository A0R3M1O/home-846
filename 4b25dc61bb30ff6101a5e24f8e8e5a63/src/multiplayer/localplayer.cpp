/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * @author Aly Cardinal
 * @link https://vk.com/id650525154 
 * @community https://vk.com/a.home846
 * 
*/

#include "main.h"
#include "core/core.h"
#include "multiplayer.h"

localplayer::localplayer()
{
    g_pPed = 0;
}

void localplayer::spawnPed()
{
    LOG("localplayer::spawnPed");

    g_pPed = core::getGame()->FindPlayer();
    
    if(!g_pPed) {
        return;
    }
}