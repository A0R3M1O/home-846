/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * @author Aly Cardinal
 * @link https://vk.com/id650525154 
 * @community https://vk.com/a.home846
 * 
*/

#include "main.h"
#include "../gui/mainmenu.h"
#include "multiplayer.h"

int (*PauseGame)(uintptr_t thiz);
int HOOK_PauseGame(uintptr_t thiz);

bool bPauseClient = false;

int HOOK_PauseGame(uintptr_t thiz)
{
    PauseGame(thiz);
    mainmenu::toggleRender();
}

void InitKeysyncHooks()
{   
    // HookInstall(SA(0x0025C298), (uintptr_t)HOOK_PauseGame, (uintptr_t*)&PauseGame);
}