/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * @author Aly Cardinal
 * @link https://vk.com/id650525154 
 * @community https://vk.com/a.home846
 * 
*/

#include "main.h"
#include "../core/core.h"
#include "offsets.h"
#include "../gui/gui.h"
#include "../gui/window.h"
#include "network/network.h"
#include "multiplayer.h"

bool                multiplayer::m_bSuspensionEnabled;
std::vector<char>   multiplayer::m_PlayerImgCache;
bool                multiplayer::m_bEnabledLODSystem;
bool                multiplayer::m_bEnabledAltWaterOrder;
bool                multiplayer::m_bEnabledClothesMemFix;
float               multiplayer::m_fAircraftMaxHeight;
float               multiplayer::m_fAircraftMaxVelocity;
float               multiplayer::m_fAircraftMaxVelocity_Sq;
bool                multiplayer::m_bHeatHazeEnabled;
bool                multiplayer::m_bHeatHazeCustomized;
float               multiplayer::m_fNearClipDistance;

eAnimGroup          multiplayer::m_dwLastStaticAnimGroupID;
eAnimID             multiplayer::m_dwLastStaticAnimID;
DWORD               multiplayer::m_dwLastAnimArrayAddress;

uintptr_t           multiplayer::HOOKPOS_CHud_Draw;
uintptr_t           multiplayer::HOOKPOS_RenderQueue_ProcessCommand;

uintptr_t           multiplayer::FUNC_CFont_AsciiToGxtChar;
uintptr_t           multiplayer::FUNC_CFont_PrintString;
uintptr_t           multiplayer::FUNC_CFont_SetColor;
uintptr_t           multiplayer::FUNC_CFont_SetDropColor;
uintptr_t           multiplayer::FUNC_CFont_SetEdge;
uintptr_t           multiplayer::FUNC_CFont_SetJustify;
uintptr_t           multiplayer::FUNC_CFont_SetScale;
uintptr_t           multiplayer::FUNC_CFont_SetScaleXY;
uintptr_t           multiplayer::FUNC_CFont_SetOrientation;
uintptr_t           multiplayer::FUNC_CFont_SetFontStyle;
uintptr_t           multiplayer::FUNC_CFont_SetProportional;
uintptr_t           multiplayer::FUNC_CFont_SetRightJustifyWrap;
uintptr_t           multiplayer::FUNC_CFont_SetBackground;
uintptr_t           multiplayer::FUNC_CFont_SetBackgroundColor;
uintptr_t           multiplayer::FUNC_CFont_SetWrapx;
uintptr_t           multiplayer::FUNC_CFont_SetCentreSize;
uintptr_t           multiplayer::FUNC_CFont_SetDropShadowPosition;
uintptr_t           multiplayer::FUNC_CFont_GetHeight;
uintptr_t           multiplayer::FUNC_CFont_GetStringWidth;
uintptr_t           multiplayer::FUNC_CFont_GetTextRect;

uintptr_t           multiplayer::FUNC_CFont_SetScaleY;
uintptr_t           multiplayer::FUNC_CFont_SetScaleX;

uintptr_t           multiplayer::FUNC_CFont_RenderFontBuffer;

uintptr_t           multiplayer::VAR_RS_GLOBAL;
uintptr_t           multiplayer::FUNC_RwCameraBeginUpdate;
uintptr_t           multiplayer::FUNC_RwCameraEndUpdate;
uintptr_t           multiplayer::FUNC_RwCameraShowRaster;
uintptr_t           multiplayer::FUNC_RwRasterCreate;
uintptr_t           multiplayer::FUNC_RwRasterDestroy;
uintptr_t           multiplayer::FUNC_RwRasterGetOffset;
uintptr_t           multiplayer::FUNC_RwRasterGetNumLevels;
uintptr_t           multiplayer::FUNC_RwRasterSubRaster;
uintptr_t           multiplayer::FUNC_RwRasterRenderFast;
uintptr_t           multiplayer::FUNC_RwRasterRender;
uintptr_t           multiplayer::FUNC_RwRasterRenderScaled;
uintptr_t           multiplayer::FUNC_RwRasterPushContext;
uintptr_t           multiplayer::FUNC_RwRasterPopContext;
uintptr_t           multiplayer::FUNC_RwRasterGetCurrentContext;
uintptr_t           multiplayer::FUNC_RwRasterClear;
uintptr_t           multiplayer::FUNC_RwRasterClearRect;
uintptr_t           multiplayer::FUNC_RwRasterShowRaster;
uintptr_t           multiplayer::FUNC_RwRasterLock;
uintptr_t           multiplayer::FUNC_RwRasterUnlock;
uintptr_t           multiplayer::FUNC_RwRasterLockPalette;
uintptr_t           multiplayer::FUNC_RwRasterUnlockPalette;
uintptr_t           multiplayer::FUNC_RwImageCreate;
uintptr_t           multiplayer::FUNC_RwImageDestroy;
uintptr_t           multiplayer::FUNC_RwImageAllocatePixels;
uintptr_t           multiplayer::FUNC_RwImageFreePixels;
uintptr_t           multiplayer::FUNC_RwImageCopy;
uintptr_t           multiplayer::FUNC_RwImageResize;
uintptr_t           multiplayer::FUNC_RwImageApplyMask;
uintptr_t           multiplayer::FUNC_RwImageMakeMask;
uintptr_t           multiplayer::FUNC_RwImageReadMaskedImage;
uintptr_t           multiplayer::FUNC_RwImageRead;
uintptr_t           multiplayer::FUNC_RwImageWrite;
uintptr_t           multiplayer::FUNC_RwImageSetFromRaster;
uintptr_t           multiplayer::FUNC_RwRasterSetFromImage;
uintptr_t           multiplayer::FUNC_RwRasterRead;
uintptr_t           multiplayer::FUNC_RwRasterReadMaskedRaster;
uintptr_t           multiplayer::FUNC_RwImageFindRasterFormat;
uintptr_t           multiplayer::FUNC_RwTextureCreate;
uintptr_t           multiplayer::FUNC__rwStreamInitialize;
uintptr_t           multiplayer::FUNC_RwStreamOpen;
uintptr_t           multiplayer::FUNC_RwStreamClose;
uintptr_t           multiplayer::FUNC_RwStreamRead;
uintptr_t           multiplayer::FUNC_RwStreamWrite;
uintptr_t           multiplayer::FUNC_RwStreamSkip;
uintptr_t           multiplayer::FUNC_RwIm2DGetNearScreenZ;
uintptr_t           multiplayer::FUNC_RwIm2DGetFarScreenZ;
uintptr_t           multiplayer::FUNC_RwRenderStateGet;
uintptr_t           multiplayer::FUNC_RwRenderStateSet;
uintptr_t           multiplayer::FUNC_RwIm2DRenderLine;
uintptr_t           multiplayer::FUNC_RwIm2DRenderTriangle;
uintptr_t           multiplayer::FUNC_RwIm2DRenderPrimitive;
uintptr_t           multiplayer::FUNC_RwIm2DRenderIndexedPrimitive;
uintptr_t           multiplayer::FUNC_RtPNGImageWrite;
uintptr_t           multiplayer::FUNC_RtPNGImageRead;
uintptr_t           multiplayer::FUNC_ExecuteScriptBuf;
uintptr_t           multiplayer::FUNC_TextureDatabaseRuntime_GetTexture;
uintptr_t           multiplayer::FUNC_OperatorNew;
uintptr_t           multiplayer::VAR_WORLD_PLAYERS;
uintptr_t           multiplayer::ADDR_SHADOWCRASH1;
uintptr_t           multiplayer::ADDR_SHADOWCRASH2;
uintptr_t           multiplayer::ADDR_SHADOWCRASH3;
uintptr_t           multiplayer::ADDR_COLLISION_POOL1;
uintptr_t           multiplayer::ADDR_COLLISION_POOL2;
uintptr_t           multiplayer::HOOKPOS_RwCameraSetNearClipPlane;
uintptr_t           multiplayer::HOOKPOS_RwCameraSetFarClipPlane;

float               multiplayer::m_fFarClipDistance;

bool                multiplayer::m_bAllowSinglePeds;
bool                multiplayer::m_bAllowSingleCars;

extern multiplayer *pMultiPlayer;

 void (*Idle)();
 void (*FrontendIdle)(uintptr_t thiz);
 void HOOK_Idle();

 void (*RwCameraSetNearClipPlane)(int a1, float fDistance);
 void HOOK_RwCameraSetNearClipPlane(int a1, float fDistance);
 void (*RwCameraSetFarClipPlane)(int a1, float fDistance);
 void HOOK_RwCameraSetFarClipPlane(int a1, float fDistance);
 void (*CStreaming__Init2)();
 void HOOK_CStreaming__Init2();
 void HOOK_FrontendIdle(uintptr_t thiz);

bool multiplayer::m_bRenderInited = false;

window* pMainWindow = 0;

multiplayer::multiplayer()
{
    LOG("initializing multiplayer..");

    switch(core::getGame()->FindGameVersion()) 
    {
        default:
        case VERSION_A_108: {
            LOG("Game version is 1.08");
            offsets::initialize_1_08();
            break;
        }

        case VERSION_A_200: {
            LOG("Game version is 2.00");
            offsets::initialize_2_00();
            break;
        }
    }

    multiplayer::m_bSuspensionEnabled = true;
    multiplayer::m_fAircraftMaxHeight = 800.0f;
    multiplayer::m_fAircraftMaxVelocity = 1.5f;
    multiplayer::m_fAircraftMaxVelocity_Sq = m_fAircraftMaxVelocity * m_fAircraftMaxVelocity;
    multiplayer::m_bHeatHazeEnabled = true;
    multiplayer::m_bHeatHazeCustomized = false;
    
    multiplayer::m_bAllowSinglePeds = false;
    multiplayer::m_bAllowSingleCars = false;

    multiplayer::m_dwLastStaticAnimGroupID = eAnimGroup::ANIM_GROUP_DEFAULT;
    multiplayer::m_dwLastStaticAnimID = eAnimID::ANIM_ID_WALK;

    // Scripting
    InitScripting();

    // Render
    multiplayer::initRender();

    // LOCAL PLAYER
    g_pLocalPlayer = new localplayer();
}

multiplayer::~multiplayer()
{
    DestroyGUI();
    multiplayer::m_bRenderInited = false;

    if(g_pLocalPlayer) 
    {
        delete g_pLocalPlayer;
        g_pLocalPlayer = 0;
    }
}

void multiplayer::initRender()
{
    if(!multiplayer::m_bRenderInited)
    {
        // Init RW and GUI
        InitRenderWareFunctions();
        InitGUI();

        // create main window
        ImGuiIO &io = ImGui::GetIO();
        pMainWindow = new window(1, 0.0f, 0.0f, io.DisplaySize.x, io.DisplaySize.y, ImColor(0, 0, 0, 255), ImColor(255, 255, 255, 255), true, true);
        
        // init main menu
        mainmenu::initialise();

        multiplayer::m_bRenderInited = true;
    }
}

void (*TouchEvent)(int type, int num, int posX, int posY);
void HOOK_TouchEvent(int type, int num, int posX, int posY)
{
    if(pMainWindow) 
    {
        if(!pMainWindow->touch((float)posX, (float)posY, (uint8_t)type)) {
            return;
        }
    }

    return TouchEvent(type, num, posX, posY);
}

char* WORLD_PLAYERS = nullptr;
void multiplayer::InitHooksAndPatches()
{
    LOG("initializing multiplayer hooks and patches..");

    InitKeysyncHooks();
    InitShotsyncHooks();

    multiplayer::m_fFarClipDistance = DEFAULT_FAR_CLIP_DISTANCE;
    multiplayer::m_fNearClipDistance = DEFAULT_NEAR_CLIP_DISTANCE;

    // FrontendIdle(void)	.text	0039B0C4	000000BC	00000018	00000000	R	.	.	.	B	T	.
    // Idle(void *,bool)	.text	0039B1A4	00000348	00000038	00000000	R	.	.	.	B	T	.

    // Render2dStuff(void)	.text	0039AEF4	000000DC	00000010	00000000	R	.	.	.	B	T	.

    HookInstall(SA(0x0039AEF4), (uintptr_t)HOOK_Idle, (uintptr_t*)&Idle);
    HookInstall(multiplayer::HOOKPOS_RwCameraSetNearClipPlane, (uintptr_t)HOOK_RwCameraSetNearClipPlane, (uintptr_t*)&RwCameraSetNearClipPlane);
    HookInstall(multiplayer::HOOKPOS_RwCameraSetFarClipPlane, (uintptr_t)HOOK_RwCameraSetFarClipPlane, (uintptr_t*)&RwCameraSetFarClipPlane);
    

    // todo: offsets
    HookInstall(SA(0x004042A8), (uintptr_t)HOOK_CStreaming__Init2, (uintptr_t*)&CStreaming__Init2);
    HookInstall(SA(0x00239D5C), (uintptr_t)HOOK_TouchEvent, (uintptr_t*)&TouchEvent);
    // HookInstall(SA(0x0039B0C4), (uintptr_t)HOOK_FrontendIdle, (uintptr_t*)&FrontendIdle);

    WORLD_PLAYERS = ((char* (*)(uint32_t))(multiplayer::FUNC_OperatorNew))(404 * 257 * sizeof(char));
	memset(WORLD_PLAYERS, 0, 404 * 257);

	UnFuck(multiplayer::VAR_WORLD_PLAYERS);
	*(char**)(multiplayer::VAR_WORLD_PLAYERS) = WORLD_PLAYERS;
	LOG("WORLD_PLAYERS = 0x%X", WORLD_PLAYERS);

    // Shadow crash fix
	NOP(multiplayer::ADDR_SHADOWCRASH1, 2);
	NOP(multiplayer::ADDR_SHADOWCRASH2, 2);
	NOP(multiplayer::ADDR_SHADOWCRASH3, 2);
// TODO: offsets

    // disable telemetry
    WriteMemory(SA(0x26D9B8), (uintptr_t)"\xF7\x46", 2);
    WriteMemory(SA(0x26DA20), (uintptr_t)"\xF7\x46", 2);
    WriteMemory(SA(0x26DCA4), (uintptr_t)"\xF7\x46", 2);

    // SaveGameForPause
	WriteMemory(SA(0x56C42C), (uintptr_t)"\xF7\x46", 2);

    // CInterestingEvents::ScanForNearbyEntities
    WriteMemory(SA(0x44FF34), (uintptr_t)"\x00\x20\xF7\x46", 4);

	// CGangWars::Update
    WriteMemory(SA(0x2C8AAC), (uintptr_t)"\x00\x20\xF7\x46", 4);

    // CConversations::Update
    WriteMemory(SA(0x2BE6F0), (uintptr_t)"\xF7\x46", 2);

    // CPedToPlayerConversations::Update
    WriteMemory(SA(0x2BE774), (uintptr_t)"\x00\x20\xF7\x46", 4);

	// CObject::ProcessTrainCrossingBehaviour
    WriteMemory(SA(0x3ED1B4), (uintptr_t)"\xF7\x46", 2);

	// CObject::ProcessSamSiteBehaviour
    WriteMemory(SA(0x3ECD14), (uintptr_t)"\x00\x20\xF7\x46", 4);

	// CPlantMgr::Update
	WriteMemory(SA(0x28E164), (uintptr_t)"\xF7\x46", 2); 

    // CCutsceneMgr::Update
	WriteMemory(SA(0x33FBFC), (uintptr_t)"\xF7\x46", 2);

    // CVehicle::DoDriveByShootings
	WriteMemory(SA(0x56A490), (uintptr_t)"\x00\x20\xF7\x46", 4);  

	// CGameLogic::CalcDistanceToForbiddenTrainCrossing
	WriteMemory(SA(0x2C47C8), (uintptr_t)"\xF7\x46", 2);  

	// CWanted::UpdateEachFrame
	WriteMemory(SA(0x3C1314), (uintptr_t)"\xF7\x46", 2);  

    // CWanted::Update
    WriteMemory(SA(0x3C0624), (uintptr_t)"\x00\x20\xF7\x46", 4);  

	// CCheat::DoCheats
	WriteMemory(SA(0x2BC24C), (uintptr_t)"\xF7\x46", 2); 

	// CPlayerInfo::MakePlayerSafe
	WriteMemory(SA(0x3ACBD0), (uintptr_t)"\x00\x20\xF7\x46", 4); 

	// CGameLogic::SetPlayerWantedLevelForForbiddenTerritories
    WriteMemory(SA(0x2C4694), (uintptr_t)"\x00\x20\xF7\x46", 4); 

    // CCrime::ReportCrime
    WriteMemory(SA(0x38FE34), (uintptr_t)"\xF7\x46", 2); 

	// CGarage::TidyUpGarage
	WriteMemory(SA(0x2CAD98), (uintptr_t)"\x00\x20\xF7\x46", 4); 

	// CGarage::TidyUpGarageClose
	WriteMemory(SA(0x2CBFF4), (uintptr_t)"\xF7\x46", 2); 

    // CAERadioTrackManager::CheckForMissionStatsChanges
    WriteMemory(SA(0x34EE3C), (uintptr_t)"\x00\x20\xF7\x46", 4); 

    // CAEVehicleAudioEntity::PlayerAboutToExitVehicleAsDriver
    WriteMemory(SA(0x35BDE4), (uintptr_t)"\x70\x47", 2);

    // CAEVehicleAudioEntity::JustWreckedVehicle
    WriteMemory(SA(0x35D2F0), (uintptr_t)"\x70\x47", 2);

    // CAEVehicleAudioEntity::TurnOnRadioForVehicle
    WriteMemory(SA(0x35BD4C), (uintptr_t)"\x70\x47", 2);

    // CAudioEngine::StartRadio                                           
    NOP(SA(0x265EF4), 2); 

    // CAudioEngine::Service
    NOP(SA(0x265EFA), 2); 

    // CAudioEngine::StopRadio
    NOP(SA(0x265F04), 2); 

    // CAudioEngine::Service
    NOP(SA(0x265F0A), 2); 

    // CAudioEngine::RetuneRadio
    NOP(SA(0x265F16), 2);    

	// NOP calling CMessages::AddBigMessage from CPlayerInfo::KillPlayer
	NOP(SA(0x3AC8B2), 2);

    // CFire::Extinguish
    WriteMemory(SA(0x458D68), (uintptr_t)"\x00\x46\x00\x46", 4);

    // Disable in-game radio
	NOP(SA(0x3688DA), 2);
	NOP(SA(0x368DF0), 2);
	NOP(SA(0x369072), 2);
	NOP(SA(0x369168), 2);

	// Stop it trying to load tracks2.dat
	NOP(SA(0x508F36), 2);

	// Stop it trying to load tracks4.dat
	NOP(SA(0x508F54), 2);

	// CHeli::UpdateHelis
    NOP(SA(0x398768), 2);               
    NOP(SA(0x3987DC), 2); 

    // CAEGlobalWeaponAudioEntity::ServiceAmbientGunFire              
    NOP(SA(0x3688EC), 2);     

	// Disable Map legend
	WriteMemory(SA(0x3DA500), (uintptr_t)"\xF7\x46", 2);
	NOP(SA(0x26B504), 2); 
	NOP(SA(0x26B214), 2);

	// Disable cinematic camera for trains
	UnFuck(SA(0x385D6A)); 
	*(uint8_t*)(SA(0x385D6A)) = 0;

    // Increase pickup distance visibility
	UnFuck(SA(0x2D6CC8));
	*(float*)(SA(0x2D6CC8)) = 10000.0f;

    // disable vehicle gifts
	UnFuck(SA(0x50FF70)); 
	*(uint32_t*)(SA(0x50FF70)) = 0;

    // CTheZones::ZonesVisited[100]
    UnFuck(SA(0x8EA7B0)); 
	memset((void*)(SA(0x8EA7B0)), 1, 100);

	// CTheZones::ZonesRevealed
    UnFuck(SA(0x8EA7A8)); 
	*(uint32_t*)(SA(0x8EA7A8)) = 100;

    // single stuff / TODO: offsets
    DisableSinglePeds(!multiplayer::m_bAllowSinglePeds);
    DisableSingleCars(!multiplayer::m_bAllowSingleCars);

    InitHooks_CrashFixHacks();
    Init_13();
}

void multiplayer::DisableSinglePeds(bool bDisable)
{
    // TODO: offsets
    // CPopulation::AddPed
    WriteMemory(SA(0x45F1A4), bDisable ? (uintptr_t)"\x00\x20\xF7\x46" : (uintptr_t)"\x2D\xE9\xF0\x4F", 4); 

	// CPopulation::AddPedAtAttractor
	WriteMemory(SA(0x4611C0), bDisable ? (uintptr_t)"\xF7\x46" : (uintptr_t)"\x82\xB0", 2); 

	// CPopulation::ManagePed
	WriteMemory(SA(0x45E094), bDisable ? (uintptr_t)"\x00\x20\xF7\x46" : (uintptr_t)"\x2D\xE9\xF0\x41", 4);

    // CEntryExit::GenerateAmbientPeds
    WriteMemory(SA(0x2C1CB0), bDisable ? (uintptr_t)"\x00\x20\xF7\x46" : (uintptr_t)"\x2D\xE9\xF0\x41", 4); 

    // CPopulation::GeneratePedsAtAttractors
    WriteMemory(SA(0x461334), bDisable ? (uintptr_t)"\x00\x20\xF7\x46" : (uintptr_t)"\x2D\xE9\xF0\x4F", 4); 

    // CPopulation::GeneratePedsAtStartOfGame
    WriteMemory(SA(0x4615F8), bDisable ? (uintptr_t)"\x00\x20\xF7\x46" : (uintptr_t)"\x2D\xE9\xF0\x4F", 4); 

    // CPopulation::RemovePed
    WriteMemory(SA(0x45D82C), bDisable ? (uintptr_t)"\xF7\x46" : (uintptr_t)"\xB0\xB5", 2); 

    // CPopulation::AddToPopulation 
    WriteMemory(SA(0x45FC20), bDisable ? (uintptr_t)"\x4F\xF0\x00\x00\xF7\x46" : (uintptr_t)"\x2D\xE9\xF0\x4F\x4F\xF0", 6);  

    // Stop the loading of ambient traffic models and textures
    // by skipping CStreaming::StreamVehiclesAndPeds() and CStreaming::StreamZoneModels()
    WriteMemory(SA(0x294A4C), bDisable ? (uintptr_t)"\x00\xBF\x00\xBF" : (uintptr_t)"\xFE\xF7\x26\xFA", 4);
    WriteMemory(SA(0x294A56), bDisable ? (uintptr_t)"\x00\xBF\x00\xBF" : (uintptr_t)"\x17\xF1\xC5\xFC", 4);
	WriteMemory(SA(0x294A5C), bDisable ? (uintptr_t)"\x00\xBF\x00\xBF" : (uintptr_t)"\xFD\xF7\x90\xF8", 4);

    // CStreaming::StreamVehiclesAndPeds_Always
    WriteMemory(SA(0x2923F8), bDisable ? (uintptr_t)"\xF7\x46" : (uintptr_t)"\xB0\xB5", 2);

	// NOP calling CPopulation::RemovePedsIfThePoolGetsFull after CPopulation::ManagePopulation call
    WriteMemory(SA(0x4617AC), bDisable ? (uintptr_t)"\x00\xBF\x00\xBF" : (uintptr_t)"\xFB\xF7\xF6\xFA", 4);
}

void multiplayer::DisableSingleCars(bool bDisable)
{
    // TODO: offsets
    // CTheCarGenerators::Load
    WriteMemory(SA(0x42296C), bDisable ? (uintptr_t)"\x00\x20\xF7\x46" : (uintptr_t)"\x2D\xE9\xF0\x41", 4); 
    
    // CTheCarGenerators::Process
    WriteMemory(SA(0x4F90AC), bDisable ? (uintptr_t)"\xF7\x46" : (uintptr_t)"\xB0\xB5", 2); 

    // CCarCtrl::GenerateOneEmergencyServicesCar
    WriteMemory(SA(0x2B055C), bDisable ? (uintptr_t)"\x00\x20\xF7\x46" : (uintptr_t)"\x2D\xE9\xF0\x4F", 4); 

    // CCarCtrl::GenerateEmergencyServicesCar
    WriteMemory(SA(0x2B099C), bDisable ? (uintptr_t)"\x00\x20\xF7\x46" : (uintptr_t)"\x2D\xE9\xF0\x41", 4); 

    // CCarCtrl::GenerateOneRandomCar
    WriteMemory(SA(0x2B3F08), bDisable ? (uintptr_t)"\x00\x20\xF7\x46" : (uintptr_t)"\x2D\xE9\xF0\x4F", 4); 

    // CCarCtrl::GenerateRandomCars
    WriteMemory(SA(0x2B5C24), bDisable ? (uintptr_t)"\xF7\x46" : (uintptr_t)"\xB0\xB5", 2); 

    // CCarCtrl::GenerateCarCreationCoors2
    WriteMemory(SA(0x2A4838), bDisable ? (uintptr_t)"\x00\x20\xF7\x46" : (uintptr_t)"\x2D\xE9\xF0\x4F", 4); 

    // CCarCtrl::RemoveCarsIfThePoolGetsFull
	WriteMemory(SA(0x2AEB00), bDisable ? (uintptr_t)"\xF7\x46" : (uintptr_t)"\x4E\x4B", 2);

    // CPlane::DoPlaneGenerationAndRemoval
    WriteMemory(SA(0x504DB8), bDisable ? (uintptr_t)"\x00\x20\xF7\x46" : (uintptr_t)"\x2D\xE9\xF0\x4F", 4);

    // CTrain::DoTrainGenerationAndRemoval
    WriteMemory(SA(0x50A1D8), bDisable ? (uintptr_t)"\x00\x20\xF7\x46" : (uintptr_t)"\x2D\xE9\xF0\x4F", 4);

    // CCarCtrl::RemoveDistantCars 
	WriteMemory(SA(0x2AEA10), bDisable ? (uintptr_t)"\x00\x20\xF7\x46" : (uintptr_t)"\x2D\xE9\xF0\x41", 4);  
}

void multiplayer::SetFarClipDistance(float fDistance)
{
    multiplayer::m_fFarClipDistance = fDistance;
}

float multiplayer::GetFarClipDistance()
{
    return multiplayer::m_fFarClipDistance;
}

void multiplayer::SetNearClipDistance(float fDistance)
{
    multiplayer::m_fNearClipDistance = fDistance;
}

float multiplayer::GetNearClipDistance()
{
    return multiplayer::m_fNearClipDistance;
}

void HOOK_CStreaming__Init2()
{
	CStreaming__Init2();
	*(uint32_t*)(SA(0x005DE734)) = MAX_STREAMING_MEMORY;
}

void *mtasa_graphicThread(void *p)
{	
    pthread_exit(0);
}

void HOOK_Idle()
{
    Idle();

    static bool bFirstCalled = false;

    if(!bFirstCalled) 
    {
        LOG("Main menu");

        if(pMultiPlayer) {
            pMultiPlayer->getLocalPlayer()->spawnPed();
        }

        bFirstCalled = true;
    }

    network::Process();
        
    if(multiplayer::m_bRenderInited) 
    {
       if(pMainWindow) {
           pMainWindow->draw();
       } 
    }	
}

void HOOK_FrontendIdle(uintptr_t thiz)
{
    FrontendIdle(thiz);

    if(multiplayer::m_bRenderInited) 
    {
       if(pMainWindow) {
           pMainWindow->draw();
       } 
    }	
}

void HOOK_RwCameraSetNearClipPlane(int a1, float fDistance)
{
    uintptr_t dwCalledFrom = 0;
 	__asm__ volatile ("mov %0, lr" : "=r" (dwCalledFrom));
 	dwCalledFrom -= g_GTASAHandle;

    float fSetting = pMultiPlayer->GetNearClipDistance();
    if (fSetting == DEFAULT_NEAR_CLIP_DISTANCE)
    {
        // Do nothing if setting is default value
        return RwCameraSetNearClipPlane(a1, fDistance);
    }

    // Don't process calls from RenderScene as they are restoring saved values (which have already been processed here)
    if (dwCalledFrom > 0x39ABE8 && dwCalledFrom < 0x39ADEA)
    {
        return RwCameraSetNearClipPlane(a1, fDistance);
    }

    if (fSetting < DEFAULT_NEAR_CLIP_DISTANCE)
    {
        // If required setting is lower than default, ensure value used is not higher.
        return RwCameraSetNearClipPlane(a1, std::min(fSetting, fDistance));
    }
    else
    {
        // If required setting is higher than default, converge value towards it.
        float fAlpha = UnlerpClamped(DEFAULT_NEAR_CLIP_DISTANCE, fSetting, DEFAULT_NEAR_CLIP_DISTANCE * 3);
        return RwCameraSetNearClipPlane(a1, Lerp(fDistance, fAlpha, fSetting));
    }

    return RwCameraSetNearClipPlane(a1, fDistance);
}

void HOOK_RwCameraSetFarClipPlane(int a1, float fDistance)
{
    float fSetting = pMultiPlayer->GetFarClipDistance();

    if (fSetting < DEFAULT_NEAR_CLIP_DISTANCE) {
        return RwCameraSetFarClipPlane(a1, std::min(fSetting, fDistance));
    }

    return RwCameraSetFarClipPlane(a1, fSetting);
}