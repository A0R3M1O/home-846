/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * @author Aly Cardinal
 * @link https://vk.com/id650525154 
 * @community https://vk.com/a.home846
 * 
*/

#include "../main.h"
#include "core/core.h"
#include "game/game.h"
#include "multiplayer.h"
#include "multiplayer_init.h"
#include "utilities/fpsfix.h"

uint8_t fps = MAX_FPS;
uint8_t fps_limit = MIN_FPS;

multiplayer     *pMultiPlayer = 0;

uintptr_t HOOKPOS_MainMenuScreen_Update = 0;
uintptr_t HOOKPOS_ANDRunThread = 0;
uintptr_t HOOKPOS_OS_MutexRelease = 0;
uintptr_t ADDR_STR_ANDVIEW = 0;
uintptr_t ADDR_STR_EXITSC = 0;
uintptr_t ADDR_STR_SCV = 0;
uintptr_t ADDR_SC_SBUTTON = 0;
uintptr_t ADDR_FPS_1 = 0;
uintptr_t ADDR_FPS_2 = 0;
uintptr_t ADDR_FPS_3 = 0;
uintptr_t ADDR_FPS_4 = 0;
uintptr_t ADDR_FPS_LIMIT = 0;
uintptr_t ADDR_TextureDatabaseRuntime_LoadFullTexture = 0;
uintptr_t ADDR_Interior_c_Init = 0;
uintptr_t ADDR_RENDER_MEMORY_BUFFER1 = 0;
uintptr_t ADDR_RENDER_MEMORY_BUFFER2 = 0;
uintptr_t ADDR_CCarFXRenderer_Shutdown1 = 0;
uintptr_t ADDR_CCarFXRenderer_Shutdown2 = 0;
uintptr_t ADDR_CStreaming_Shutdown = 0;
uintptr_t ADDR_CUpsideDownCarCheck_UpdateTimers = 0;
uintptr_t ADDR_CRoadBlocks_GenerateRoadBlocks = 0;
uintptr_t ADDR_RQS = 0;
uintptr_t ADDR_eglGetProcAddress = 0;
uintptr_t ADDR_RQ_Command_rqSetAlphaTest = 0;
uintptr_t ADDR_OS_MUTEX_COND = 0;
uintptr_t ADDR_PICKUPSGENERATOR1 = 0;
uintptr_t ADDR_PICKUPSGENERATOR2 = 0;
uintptr_t ADDR_CAnimManager__RemoveAnimBlockRef = 0;

unsigned int (*OS_MutexRelease)(void* a1);
unsigned int HOOK_OS_MutexRelease(void* a1);

void (*ANDRunThread)(void* a1);
void HOOK_ANDRunThread(void* a1);

unsigned int (*MainMenuScreen_Update)(uintptr_t thiz, float a2);
unsigned int HOOK_MainMenuScreen_Update(uintptr_t thiz, float a2);

bool (*CGame_Shutdown)();
bool HOOK_CGame_Shutdown();

static CFPSFix g_fpsFix;
static pthread_mutex_t *mutex = 0;

int iGameVersion = 0;

void InitMultiPlayer()
{
	static bool bMultiPlayerInited = false;

	if(!bMultiPlayerInited)
	{
		// initialize multiplayer
		pMultiPlayer = new multiplayer();
		pMultiPlayer->InitHooksAndPatches();

		CClient::ProcessCommand("start");

		bMultiPlayerInited = true;
	}
}

void InitGlobalHooks()
{  
    HookInstall(HOOKPOS_ANDRunThread, (uintptr_t)HOOK_ANDRunThread, (uintptr_t*)&ANDRunThread);
    HookInstall(HOOKPOS_MainMenuScreen_Update, (uintptr_t)HOOK_MainMenuScreen_Update, (uintptr_t*)&MainMenuScreen_Update);
    HookInstall(HOOKPOS_OS_MutexRelease, (uintptr_t)HOOK_OS_MutexRelease, (uintptr_t*)&OS_MutexRelease);
	HookInstall(SA(0x00398334), (uintptr_t)HOOK_CGame_Shutdown, (uintptr_t*)&CGame_Shutdown);
}

void InitialisePools()
{
    if(iGameVersion == 108)
    { // 1.08
        // CPed pool (old: 140, new: 210)
	    /* 	MOVW R0, #0x5EC8
	    	MOVT R0, #6 */
	    WriteMemory(SA(0x3AF2D0), (uintptr_t)"\x45\xF6\xC8\x60\xC0\xF2\x06\x00", 8); // MOV  R0, #0x65EC8 | size=0x7C4 (old: 0x43F30)
	    WriteMemory(SA(0x3AF2DE), (uintptr_t)"\xD2\x20", 2); // MOVS R0, #0xD2
	    WriteMemory(SA(0x3AF2E4), (uintptr_t)"\xD2\x22", 2); // MOVS R2, #0xD2
	    WriteMemory(SA(0x3AF310), (uintptr_t)"\xD2\x2B", 2); // CMP  R3, #0xD2

	    // CPedIntelligence pool (old: 140, new: 210)
	    // movw r0, #0x20B0
	    // movt r0, #2
	    // nop
	    WriteMemory(SA(0x3AF7E6), (uintptr_t)"\x42\xF2\xB0\x00\xC0\xF2\x02\x00\x00\x46", 10); // MOVS R0, #0x220B0 | size=0x298 (old: 0x16B20)
	    WriteMemory(SA(0x3AF7F6), (uintptr_t)"\xD2\x20", 2); // MOVS R0, #0xD2
	    WriteMemory(SA(0x3AF7FC), (uintptr_t)"\xD2\x22", 2); // MOVS R2, #0xD2
	    WriteMemory(SA(0x3AF824), (uintptr_t)"\xD2\x2B", 2); // CMP  R3, 0xD2

	    // Task pool (old: 500, new: 1524 (1536))
	    WriteMemory(SA(0x3AF4EA), (uintptr_t)"\x4F\xF4\x40\x30", 4); // MOV.W R0, #30000 | size = 0x80 (old: 0xFA00)
	    WriteMemory(SA(0x3AF4F4), (uintptr_t)"\x4F\xF4\xC0\x60", 4); // MOV.W R0, #0x600
	    WriteMemory(SA(0x3AF4FC), (uintptr_t)"\x4F\xF4\xC0\x62", 4); // MOV.W R2, #0x600
	    WriteMemory(SA(0x3AF52A), (uintptr_t)"\xB3\xF5\xC0\x6F", 4); // CMP.W R3, #0x600

	    // TxdStore pool (old: 5000, new: 20000)
	    WriteMemory(SA(0x55BA9A), (uintptr_t)"\x4f\xf4\xb8\x50\xc0\xf2\x11\x00", 8); //  MOV.W	R0, #0x1700 | MOVT	R0, #0x11
	    WriteMemory(SA(0x55BAA8), (uintptr_t)"\x44\xf6\x20\x60", 4); // MOVW	R0, #0x4E20
	    WriteMemory(SA(0x55BAB0), (uintptr_t)"\x44\xf6\x20\x62", 4); // MOVW	R2, #0x4E20

	    // Object pool (old: 350, new: 2992)
	    WriteMemory(SA(0x3AF3D6), (uintptr_t)"\x4f\xf4\x7e\x40\xc0\xf2\x12\x00", 8); // MOV.W	R0, #0x5900 | MOVT	R0, #0x6
	    WriteMemory(SA(0x3AF3E4), (uintptr_t)"\x4f\xf4\x3b\x60", 4); // MOV.W R0, #0xBB0
	    WriteMemory(SA(0x3AF3EC), (uintptr_t)"\x4f\xf4\x3b\x62", 4); // MOV.W R2, #0xBB0
	    WriteMemory(SA(0x3AF41A), (uintptr_t)"\xb3\xf5\x3b\x6f", 4); // CMP.W R3, #0xBB0

	    // Building pool (old: 14000, new: 29585)
	    WriteMemory(SA(0x3AF378), (uintptr_t)"\x4F\xF4\x90\x40\xC0\xF2\x19\x00", 8); // mov.w r0, #0x4800 movt r0, #0x19
	    WriteMemory(SA(0x3AF386), (uintptr_t)"\x47\xF2\x91\x30", 4); // mov r0, #0x7391
	    WriteMemory(SA(0x3AF38E), (uintptr_t)"\x47\xF2\x91\x32", 4); //mov r2, #0x7391

	    // Dummys pool (old: 3500, new: 4000)
	    WriteMemory(SA(0x3AF430), (uintptr_t)"\x4f\xf4\xd6\x40\xc0\xf2\x03\x00", 8); // MOV.W	R0, #0x6B00 | MOVT	R0, #0x3
	    WriteMemory(SA(0x3AF43E), (uintptr_t)"\x40\xf6\xa0\x70", 4); // MOVW            R0, #0xFA0
	    WriteMemory(SA(0x3AF446), (uintptr_t)"\x40\xf6\xa0\x72", 4); // MOVW            R2, #0xFA0

	    // PtrNode Single pool (old: 75000, new: 100000)
	    WriteMemory(SA(0x3AF1BC), (uintptr_t)"\x4f\xf4\x54\x50\xc0\xf2\x0c\x00", 8); // MOV.W	R0, #0x3500 | MOVT	R0, #0xC
	    WriteMemory(SA(0x3AF1D6), (uintptr_t)"\x48\xf2\xa0\x62", 4); // MOVW	R2, #0x86A0
	    WriteMemory(SA(0x3AF1B0), (uintptr_t)"\x48\xf2\xa0\x66", 4); // MOVW	R6, #0x86A0
	    WriteMemory(SA(0x3AF1CA), (uintptr_t)"\x48\xf2\xa0\x60\xc0\xf2\x01\x00", 8); // MOV.W R0, #0x86A0 | MOVT R0, #1

	    // PtrNode Double pool (old: 6000, new: 8000)
	    WriteMemory(SA(0x3AF21C), (uintptr_t)"\x4f\xf4\xee\x40\xc0\xf2\x01\x00", 8); // MOV.W	R0, #0x7700 | MOVT	R0, #1
	    WriteMemory(SA(0x3AF22A), (uintptr_t)"\x41\xf6\x40\x70", 4); // MOVW            R0, #8000
	    WriteMemory(SA(0x3AF232), (uintptr_t)"\x41\xf6\x40\x72", 4); // MOVW            R2, #8000

	    // Entry Node Info pool (old: 500, new: 5120)
	    WriteMemory(SA(0x3AF27A), (uintptr_t)"\x4f\xf4\xc8\x30", 4); // MOV.W	R0, #0x19000 | size = 0x14
	    WriteMemory(SA(0x3AF284), (uintptr_t)"\x4f\xf4\xa0\x50", 4); // MOV.W R0, #0x1400
	    WriteMemory(SA(0x3AF28C), (uintptr_t)"\x4f\xf4\xa0\x52", 4); // MOV.W R2, #0x1400
	    WriteMemory(SA(0x3AF2BA), (uintptr_t)"\xb3\xf5\xa0\x5f", 4); // CMP.W R3, #0x1400

        // ColModel pool (old:10150, new: 87266)
	    // mov r0, #0xEA60
 	    // movt r0, #0x3F
	    WriteMemory(SA(0x3AF48E), (uintptr_t)"\x4E\xF6\x60\x20\xC0\xF2\x3F\x00", 8); // movw r0, #0x3FEA60 | size = 0x30
	    WriteMemory(SA(0x3AF49C), (uintptr_t)"\x4D\xF2\xF0\x20", 4); // movw r0, #54000
	    WriteMemory(SA(0x3AF4A4), (uintptr_t)"\x4D\xF2\xF0\x22", 4); // movw r2, #54000

	    // VehicleStruct increase 
	    WriteMemory(SA(0x405338), (uintptr_t)"\x4F\xF4\x00\x30", 4);	// MOV  R0, #0x20000
	    WriteMemory(SA(0x405342), (uintptr_t)"\xF7\x20", 2);			// MOVS R0, #0xF7
	    WriteMemory(SA(0x405348), (uintptr_t)"\xF7\x22", 2);			// MOVS R2, #0xF7
	    WriteMemory(SA(0x405374), (uintptr_t)"\xF7\x2B", 2);			// CMP  R3, #0xF7

        // ms_pPointRoutePool
        WriteMemory(SA(0x3AF58C), (uintptr_t)"\x4F\xF4\x15\x40", 4); // mov.w r0, #0x9500 
        WriteMemory(SA(0x3AF596), (uintptr_t)"\xFF\x20", 2); // movs r0, #0xFF 
        WriteMemory(SA(0x3AF59C), (uintptr_t)"\xFF\x22", 2); // movs r2, #0xFF 
        WriteMemory(SA(0x3AF5C8), (uintptr_t)"\xFF\x2B", 2); // cmp r3, #0xFF 

        // ms_pPatrolRoutePool
        WriteMemory(SA(0x3AF5DA), (uintptr_t)"\x4F\xF4\xEA\x40", 4); // mov.w r0, #0x7500 
        WriteMemory(SA(0x3AF5E4), (uintptr_t)"\xFF\x20", 2); // movs r0, #0xFF 
        WriteMemory(SA(0x3AF5EA), (uintptr_t)"\xFF\x22", 2); // movs r2, #0xFF 
        WriteMemory(SA(0x3AF616), (uintptr_t)"\xFF\x2B", 2); // cmp r3, #0xFF  

        // ms_pNodeRoutePool
        WriteMemory(SA(0x3AF628), (uintptr_t)"\x4F\xF4\xA0\x40", 4); // mov.w r0, #0x5000 
        WriteMemory(SA(0x3AF632), (uintptr_t)"\xFF\x20", 2); // movs r0, #0xFF 
        WriteMemory(SA(0x3AF638), (uintptr_t)"\xFF\x22", 2); // movs r2, #0xFF 
        WriteMemory(SA(0x3AF664), (uintptr_t)"\xFF\x2B", 2); // cmp r3, #0xFF  

        // ms_pEventPool
        WriteMemory(SA(0x3AF53E), (uintptr_t)"\x4D\xF6\xC0\x20", 4);  // movw r0, #DAC0 
        WriteMemory(SA(0x3AF548), (uintptr_t)"\xFF\x20", 2); // movs r0, #0xFF 
        WriteMemory(SA(0x3AF54E), (uintptr_t)"\xFF\x22", 2); // movs r2, #0xFF 
        WriteMemory(SA(0x3AF57A), (uintptr_t)"\xFF\x2B", 2); // cmp r3, #0xFF  

        // ms_pTaskAllocatorPool
        WriteMemory(SA(0x3AF67C), (uintptr_t)"\x4F\xF4\x00\x50", 4); // mov.w r0, #0x2000 
        WriteMemory(SA(0x3AF686), (uintptr_t)"\xFF\x20", 2); // movs r0, #0xFF 
        WriteMemory(SA(0x3AF692), (uintptr_t)"\xFF\x22", 2); // movs r2, #0xFF 

	    // CColStore::ms_pColPool
        WriteMemory(SA(0x29EEBA), (uintptr_t)"\x4D\xF6\xF4\x10", 4); // movw r0, #0xD9F4

        // CCollision::Init
        WriteMemory(SA(0x29554A), (uintptr_t)"\x4F\xF4\x61\x60", 4);
	    WriteMemory(SA(0x295556), (uintptr_t)"\x4F\xF4\x5B\x62", 4); 

        // Increase matrix count in CPlaceable::InitMatrixArray 
	    WriteMemory(SA(0x3ABB0A), (uintptr_t)"\x4F\xF4\x7A\x51", 4); // MOV.W R1, #16000

        // CStreaming pool
	    WriteMemory(SA(0x4045B6), (uintptr_t)"\x4D\xF2\xF0\x20", 4); // movw r0, #0xd2f0 (54000)
        WriteMemory(SA(0x4045DA), (uintptr_t)"\x4D\xF2\xE4\x22", 4); // movw r2, #0xd2e4 (53988)   
    }
    else
    { // 2.00
        // todo ~
    }
}


void InitGlobalPatches()
{
    // cut social club
	UnFuck(ADDR_STR_ANDVIEW);
    strcpy((char*)(ADDR_STR_ANDVIEW), "com/rockstargames/hal/andViewManager");
	UnFuck(ADDR_STR_EXITSC);
	strcpy((char*)(ADDR_STR_EXITSC), "staticExitSocialClub");
	UnFuck(ADDR_STR_SCV);
	strcpy((char*)(ADDR_STR_SCV), "()V");
    NOP(ADDR_SC_SBUTTON, 2);

	// FPS hack
    WriteMemory(ADDR_FPS_1, (uintptr_t)&fps, 1);
	WriteMemory(ADDR_FPS_2, (uintptr_t)&fps, 1);
	WriteMemory(ADDR_FPS_3, (uintptr_t)&fps, 1);
	WriteMemory(ADDR_FPS_4, (uintptr_t)&fps, 1);
	WriteMemory(ADDR_FPS_LIMIT, (uintptr_t)&fps_limit, 1);

    // TextureDatabaseRuntime::LoadFullTexture
    WriteMemory(ADDR_TextureDatabaseRuntime_LoadFullTexture, (uintptr_t)"\x10\x46\xA2\xF1\x04\x0B", 6);

    // Interior_c::Init
	WriteMemory(ADDR_Interior_c_Init, (uintptr_t)"\x67\xE0", 2);

	// increase render memory buffer
	WriteMemory(ADDR_RENDER_MEMORY_BUFFER1, (uintptr_t)"\x4F\xF4\x40\x10\x4F\xF4\x40\x10", 8);
	WriteMemory(ADDR_RENDER_MEMORY_BUFFER2, (uintptr_t)"\x4F\xF4\x40\x10\x4F\xF4\x40\x10", 8);

    // OS_Mutex
	WriteMemory(ADDR_OS_MUTEX_COND, (uintptr_t)"\x4F\xF0\x01\x00\x00\x46", 6);

	NOP(ADDR_CCarFXRenderer_Shutdown1, 2); // CCarFXRenderer::Shutdown
	NOP(ADDR_CCarFXRenderer_Shutdown2, 2); // CCarFXRenderer::Shutdown
	NOP(ADDR_CStreaming_Shutdown, 2); // CStreaming::Shutdown
	NOP(ADDR_CUpsideDownCarCheck_UpdateTimers, 2); // CUpsideDownCarCheck::UpdateTimers
	NOP(ADDR_CRoadBlocks_GenerateRoadBlocks, 2); // CRoadBlocks::GenerateRoadBlocks

    // disable pickups generator
	NOP(ADDR_PICKUPSGENERATOR1, 2);
	NOP(ADDR_PICKUPSGENERATOR2, 2);

    // CAnimManager::RemoveAnimBlockRef
	NOP(ADDR_CAnimManager__RemoveAnimBlockRef, 17); 

	// pause original
	// NOP(SA(0x56CD4E), 2);
    // NOP(SA(0x26A4F6), 2);
    // NOP(SA(0x39FC30), 2);
    // NOP(SA(0x3CB020), 2);
    // WriteMemory(SA(0x56CD78), (uintptr_t)"\x00\x22", 2);

    InitialisePools();

    // rqStuff
	if (!*(uintptr_t*)(ADDR_RQS))
	{
		uintptr_t test = ((uintptr_t(*)(const char*))(ADDR_eglGetProcAddress))("glAlphaFuncQCOM");
		if (!test) {
			WriteMemory(ADDR_RQ_Command_rqSetAlphaTest, (uintptr_t)"\x70\x47", 2);
		}
	}
}

void InitGlobalHooksAndPatches()
{
	LOG("initializing hooks and patches..");

    /** Select game version */
	if(core::getGame()) {
        iGameVersion = core::getGame()->FindGameVersion() == VERSION_A_108 ? 108 : 200;
	}

	if(iGameVersion == 0) 
	{
		ERROR("invalid game version!");
		return;
	}

    HOOKPOS_MainMenuScreen_Update               = iGameVersion == 108 ? SA(0x25E660) : SA(0x0);
    HOOKPOS_ANDRunThread                        = iGameVersion == 108 ? SA(0x23768C) : SA(0x0);
    HOOKPOS_OS_MutexRelease                     = iGameVersion == 108 ? SA(0x2384FC) : SA(0x0);
    ADDR_STR_ANDVIEW                            = iGameVersion == 108 ? SC(0x1E16DC) : SC(0x0);
    ADDR_STR_EXITSC                             = iGameVersion == 108 ? SC(0x1E1738) : SC(0x0);
    ADDR_STR_SCV                                = iGameVersion == 108 ? SC(0x1E080C) : SC(0x0);
    ADDR_SC_SBUTTON                             = iGameVersion == 108 ? SA(0x2665EE) : SA(0x0);
    ADDR_FPS_1                                  = iGameVersion == 108 ? SA(0x463FE8) : SA(0x0);
    ADDR_FPS_2                                  = iGameVersion == 108 ? SA(0x56C1F6) : SA(0x0);
    ADDR_FPS_3                                  = iGameVersion == 108 ? SA(0x56C126) : SA(0x0);
    ADDR_FPS_4                                  = iGameVersion == 108 ? SA(0x95B074) : SA(0x0);
    ADDR_FPS_LIMIT                              = iGameVersion == 108 ? SA(0x56C1A2) : SA(0x0);
    ADDR_TextureDatabaseRuntime_LoadFullTexture = iGameVersion == 108 ? SA(0x1BDD4A) : SA(0x0);
    ADDR_Interior_c_Init                        = iGameVersion == 108 ? SA(0x3E1A2C) : SA(0x0);
    ADDR_RENDER_MEMORY_BUFFER1                  = iGameVersion == 108 ? SA(0x1A7EF2) : SA(0x0);
    ADDR_RENDER_MEMORY_BUFFER2                  = iGameVersion == 108 ? SA(0x1A7F34) : SA(0x0);
    ADDR_CCarFXRenderer_Shutdown1               = iGameVersion == 108 ? SA(0x39844E) : SA(0x0);
    ADDR_CCarFXRenderer_Shutdown2               = iGameVersion == 108 ? SA(0x39845E) : SA(0x0);
    ADDR_CStreaming_Shutdown                    = iGameVersion == 108 ? SA(0x39840A) : SA(0x0);
    ADDR_CUpsideDownCarCheck_UpdateTimers       = iGameVersion == 108 ? SA(0x2E1EDC) : SA(0x0);
    ADDR_CRoadBlocks_GenerateRoadBlocks         = iGameVersion == 108 ? SA(0x398972) : SA(0x0);
    ADDR_RQS                                    = iGameVersion == 108 ? SA(0x61B298) : SA(0x0);
    ADDR_eglGetProcAddress                      = iGameVersion == 108 ? SA(0x179A20) : SA(0x0);
    ADDR_RQ_Command_rqSetAlphaTest              = iGameVersion == 108 ? SA(0x1A6164) : SA(0x0);
    ADDR_OS_MUTEX_COND                          = iGameVersion == 108 ? SA(0x1A7ECE) : SA(0x0);
    ADDR_PICKUPSGENERATOR1                      = iGameVersion == 108 ? SA(0x402472) : SA(0x0);
    ADDR_PICKUPSGENERATOR2                      = iGameVersion == 108 ? SA(0x3E1AF0) : SA(0x0);
    ADDR_CAnimManager__RemoveAnimBlockRef       = iGameVersion == 108 ? SA(0x454950) : SA(0x0);

    // INIT HOOKS AND PATCHES
    InitGlobalHooks();
    InitGlobalPatches();
}

void OnCrashAverted(uint uiId)
{
    core::OnCrashAverted(uiId);
}

void OnEnterCrashZone(uint uiId)
{
    core::OnEnterCrashZone(uiId);
}

bool GetDebugIdEnabled(uint uiDebugId)
{
    return core::GetDebugIdEnabled(uiDebugId);
}

void LogEvent(uint uiDebugId, const char* szType, const char* szContext, const char* szBody, uint uiAddReportLogId)
{
    core::LogEvent(uiDebugId, szType, szContext, szBody, uiAddReportLogId);
}

// ==============================================================================================
#pragma optimize("", on)

unsigned int HOOK_MainMenuScreen_Update(uintptr_t thiz, float a2)
{
	unsigned int ret = MainMenuScreen_Update(thiz, a2);
	InitMultiPlayer();
	return ret;
}

void HOOK_ANDRunThread(void* a1)
{
	g_fpsFix.PushThread(gettid());

	ANDRunThread(a1);
}

void __MTASACall(char* m, void* global, int streamMax, int sleep)
{
    static bool bStreamReversed = false;

    int stream = 0;

    if(strncmp(m, "system_stream_reverse", 0xFF) == 0 && !bStreamReversed) 
    {
        if((pthread_mutex_t*)global == mutex) 
        {
            LOG("Reverse the stream..");

            while(true) 
            {
                if(stream >= streamMax) 
                {
                    LOG("Stream reversed!");
                    bStreamReversed = true;
                    break;
                }

                std::this_thread::sleep_for(std::chrono::milliseconds(sleep));
                stream++;
            }
        }
    }
} 

unsigned int HOOK_OS_MutexRelease(void* a1)
{
    mutex = (pthread_mutex_t*)a1;
    __MTASACall("system_stream_reverse", a1, 10, 20);

    return OS_MutexRelease(a1);
}

bool HOOK_CGame_Shutdown()
{
	LOG("CGame::Shutdown");

	// nop calling PauseOpenSLES
	NOP(SA(0x00341FCC), 2); 

	// nop calling CGenericGameStorage::GenericSave
	NOP(SA(0x0046389E), 2); 

	CClient::ProcessCommand("unload");

	std::this_thread::sleep_for(std::chrono::milliseconds(1000));

	return CGame_Shutdown();
}
