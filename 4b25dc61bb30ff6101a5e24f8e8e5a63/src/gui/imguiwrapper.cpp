/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * @author Aly Cardinal
 * @link https://vk.com/id650525154 
 * @community https://vk.com/a.home846
 * 
*/


#include "main.h"
#include "imguiwrapper.h"

RwRaster* g_FontRaster = nullptr;

#define MAX_VERTEXS 30000

RwIm2DVertex* g_pVB = nullptr; // vertex buffer
RwIm2DVertex* g_pVBEnd = nullptr;
unsigned int g_VertexBufferSize = 0;
ImVec4 g_ScissorRect;

void ImGuiWrapper::RenderDrawData(ImDrawData* draw_data)
{
	const RwReal nearScreenZ = 		*(RwReal*)(SA(0x9DAA60));	// CSprite2d::NearScreenZ 009DAA60
	const RwReal recipNearClip = 	*(RwReal*)(SA(0x9DAA64));	// CSprite2d::RecipNearClip 009DAA64
	
	if (g_VertexBufferSize < MAX_VERTEXS)
	{
		if (!g_pVB || g_VertexBufferSize < draw_data->TotalVtxCount)
		{
			if (g_pVB) { delete[] g_pVB; g_pVB = nullptr; }
			g_VertexBufferSize = MAX_VERTEXS;

			if (g_VertexBufferSize >= MAX_VERTEXS)
			{
				g_VertexBufferSize = MAX_VERTEXS;
			}

			g_pVB = new RwIm2DVertex[g_VertexBufferSize];
			g_pVBEnd = &g_pVB[g_VertexBufferSize];
			if (!g_pVB)
			{
				ERROR("GUI | Error: couldn't allocate vertex buffer (size: %d)", g_VertexBufferSize);
				return;
			}
			LOG("GUI | Vertex buffer reallocated. Size: %d", g_VertexBufferSize);
		}
	}
	RwIm2DVertex* vtx_dst = g_pVB;
	int vtx_offset = 0;

	for(int n = 0; n < draw_data->CmdListsCount; n++)
	{
		const ImDrawList* cmd_list = draw_data->CmdLists[n];
        const ImDrawVert* vtx_src = cmd_list->VtxBuffer.Data;
        const ImDrawIdx* idx_src = cmd_list->IdxBuffer.Data;

		if (vtx_dst >= g_pVBEnd - 1)
		{
			return;
		}

        for(int i = 0; i < cmd_list->VtxBuffer.Size; i++)
        {
			if (vtx_dst >= g_pVBEnd - 1)
			{
				return;
			}

        	RwIm2DVertexSetScreenX(vtx_dst, vtx_src->pos.x);
        	RwIm2DVertexSetScreenY(vtx_dst, vtx_src->pos.y);
        	RwIm2DVertexSetScreenZ(vtx_dst, nearScreenZ);
        	RwIm2DVertexSetRecipCameraZ(vtx_dst, recipNearClip);
        	vtx_dst->emissiveColor = vtx_src->col;
        	RwIm2DVertexSetU(vtx_dst, vtx_src->uv.x, recipCameraZ);
        	RwIm2DVertexSetV(vtx_dst, vtx_src->uv.y, recipCameraZ);

        	vtx_dst++;
        	vtx_src++;
        }
        const ImDrawIdx* idx_buffer = cmd_list->IdxBuffer.Data;
        for(int cmd_i = 0; cmd_i < cmd_list->CmdBuffer.Size; cmd_i++)
        {
        	const ImDrawCmd* pcmd = &cmd_list->CmdBuffer[cmd_i];

        	if(pcmd->UserCallback)
        	{
        		pcmd->UserCallback(cmd_list, pcmd);
        	}
        	else
        	{
        		g_ScissorRect.x = pcmd->ClipRect.x;
        		g_ScissorRect.y = pcmd->ClipRect.w;
        		g_ScissorRect.z = pcmd->ClipRect.z;
        		g_ScissorRect.w = pcmd->ClipRect.y;

                // CWidget::SetScissor
                ((void (*)(void*))(SA(0x00273E8C + 1)))((void*)&g_ScissorRect);

        		RwRenderStateSet(rwRENDERSTATEZTESTENABLE, (void*)0);
  				RwRenderStateSet(rwRENDERSTATEZWRITEENABLE, (void*)0);
  				RwRenderStateSet(rwRENDERSTATEVERTEXALPHAENABLE, (void*)1);
  				RwRenderStateSet(rwRENDERSTATESRCBLEND, (void*)rwBLENDSRCALPHA);
  				RwRenderStateSet(rwRENDERSTATEDESTBLEND, (void*)rwBLENDINVSRCALPHA);
  				RwRenderStateSet(rwRENDERSTATEFOGENABLE, (void*)0);
  				RwRenderStateSet(rwRENDERSTATECULLMODE, (void*)rwCULLMODECULLNONE);
  				RwRenderStateSet(rwRENDERSTATEBORDERCOLOR, (void*)0);
  				RwRenderStateSet(rwRENDERSTATEALPHATESTFUNCTION, (void*)rwALPHATESTFUNCTIONGREATER);
  				RwRenderStateSet(rwRENDERSTATEALPHATESTFUNCTIONREF, (void*)2);
  				RwRenderStateSet(rwRENDERSTATETEXTUREFILTER, (void*)rwFILTERLINEAR);
  				RwRenderStateSet(rwRENDERSTATETEXTUREADDRESS, (void*)rwTEXTUREADDRESSCLAMP);
  				RwRenderStateSet(rwRENDERSTATETEXTURERASTER, (void*)pcmd->TextureId);

  				RwIm2DRenderIndexedPrimitive(rwPRIMTYPETRILIST, 
  					&g_pVB[vtx_offset], (RwInt32)cmd_list->VtxBuffer.Size,
  					(RwImVertexIndex*)idx_buffer, pcmd->ElemCount);
  				RwRenderStateSet(rwRENDERSTATETEXTURERASTER, (void*)0);
  				g_ScissorRect.x = 0;
        		g_ScissorRect.y = 0;
        		g_ScissorRect.z = 0;
        		g_ScissorRect.w = 0;

        		// CWidget::SetScissor
                ((void (*)(void*))(SA(0x00273E8C + 1)))((void*)&g_ScissorRect);
        	}

        	idx_buffer += pcmd->ElemCount;
        }
        vtx_offset += cmd_list->VtxBuffer.Size;
	}
	return;
}

bool ImGuiWrapper::Init()
{
    LOG("ImGuiWrapper::Init");

	ImGuiIO &io = ImGui::GetIO();

	io.DisplaySize = ImVec2((float)RsGlobal->maximumWidth, RsGlobal->maximumHeight);
	LOG("Display size: %f, %f", io.DisplaySize.x, io.DisplaySize.y);

	return true;
}

bool ImGuiWrapper::CreateDeviceObjects()
{
	LOG("ImGuiWrapper::CreateDeviceObjects");

	// Build texture atlas
	ImGuiIO &io = ImGui::GetIO();
	unsigned char* pxs;
	int width, height, bytes_per_pixel;
	io.Fonts->GetTexDataAsRGBA32(&pxs, &width, &height, &bytes_per_pixel);
	LOG("Font atlas width: %d, height: %d, depth: %d", width, height, bytes_per_pixel*8);

	RwImage *font_img = RwImageCreate(width, height, bytes_per_pixel*8);
	RwImageAllocatePixels(font_img);

	RwUInt8 *pixels = font_img->cpPixels;
	for(int y = 0; y < font_img->height; y++)
	{
		memcpy((unsigned char*)pixels, pxs + font_img->stride * y, font_img->stride);
		pixels += font_img->stride;
	}

	RwInt32 w, h, d, flags;
	RwImageFindRasterFormat(font_img, rwRASTERTYPETEXTURE, &w, &h, &d, &flags);
	g_FontRaster = RwRasterCreate(w, h, d, flags);
	g_FontRaster = RwRasterSetFromImage(g_FontRaster, font_img);
	RwImageDestroy(font_img);

	io.Fonts->TexID = (ImTextureID*)g_FontRaster;
	return true;
}

void ImGuiWrapper::ShutDown()
{
	LOG("ImGuiWrapper::ShutDown");

	ImGuiIO &io = ImGui::GetIO();

	// destroy raster
	RwRasterDestroy(g_FontRaster);
	g_FontRaster = nullptr;
	io.Fonts->TexID = nullptr;

	// destroy vertex buffer
	if(g_pVB) { delete g_pVB; g_pVB = nullptr; }
	return;
}

void ImGuiWrapper::NewFrame()
{
	if(!g_FontRaster)
		ImGuiWrapper::CreateDeviceObjects();
}

void ImGuiWrapper::DrawRaster(stRect *rect, uint32_t color, RwRaster *raster, stfRect *uv)
{
	static RwIm2DVertex vert[4];
	const RwReal nearScreenZ 	= *(RwReal*)(SA(0x9DAA60));	// CSprite2d::NearScreenZ
	const RwReal recipNearClip 	= *(RwReal*)(SA(0x9DAA64));	// CSprite2d::RecipNearClip

	RwIm2DVertexSetScreenX(&vert[0], rect->x1);
	RwIm2DVertexSetScreenY(&vert[0], rect->y2);
	RwIm2DVertexSetScreenZ(&vert[0], nearScreenZ);
	RwIm2DVertexSetRecipCameraZ(&vert[0], recipNearClip);
	vert[0].emissiveColor = color;
	RwIm2DVertexSetU(&vert[0], uv ? uv->x1 : 0.0f, recipNearClip);
	RwIm2DVertexSetV(&vert[0], uv ? uv->y2 : 0.0f, recipNearClip);

	RwIm2DVertexSetScreenX(&vert[1], rect->x2);
	RwIm2DVertexSetScreenY(&vert[1], rect->y2);
	RwIm2DVertexSetScreenZ(&vert[1], nearScreenZ);
	RwIm2DVertexSetRecipCameraZ(&vert[1], recipNearClip);
	vert[1].emissiveColor = color;
	RwIm2DVertexSetU(&vert[1], uv ? uv->x2 : 0.0f, recipNearClip);
	RwIm2DVertexSetV(&vert[1], uv ? uv->y2 : 0.0f, recipNearClip);

	RwIm2DVertexSetScreenX(&vert[2], rect->x1);
	RwIm2DVertexSetScreenY(&vert[2], rect->y1);
	RwIm2DVertexSetScreenZ(&vert[2], nearScreenZ);
	RwIm2DVertexSetRecipCameraZ(&vert[2], recipNearClip);
	vert[2].emissiveColor = color;
	RwIm2DVertexSetU(&vert[2], uv ? uv->x1 : 0.0f, recipNearClip);
	RwIm2DVertexSetV(&vert[2], uv ? uv->y1 : 0.0f, recipNearClip);

	RwIm2DVertexSetScreenX(&vert[3], rect->x2);
	RwIm2DVertexSetScreenY(&vert[3], rect->y1);
	RwIm2DVertexSetScreenZ(&vert[3], nearScreenZ);
	RwIm2DVertexSetRecipCameraZ(&vert[3], recipNearClip);
	vert[3].emissiveColor = color;
	RwIm2DVertexSetU(&vert[3], uv ? uv->x2 : 0.0f, recipNearClip);
	RwIm2DVertexSetV(&vert[3], uv ? uv->y1 : 0.0f, recipNearClip);

	RwRenderStateSet(rwRENDERSTATETEXTURERASTER, (void*)raster);
	RwIm2DRenderPrimitive(rwPRIMTYPETRISTRIP, vert, 4);
	RwRenderStateSet(rwRENDERSTATETEXTURERASTER, (void*)0);
}

void ImGuiWrapper::CreateSTFRect(stRect* rect, stfRect* uv, float rc1, float rc2, float rc3, float rc4, float uv1, float uv2, float uv3, float uv4)
{
	stRect tmp_rect;
	stfRect tmp_uv;

	tmp_rect.x1 = rc1;
    tmp_rect.y1 = rc2;
    tmp_rect.x2 = rc3;
    tmp_rect.y2 = rc4;
    tmp_uv.x1 = uv1;
    tmp_uv.y1 = uv2;
    tmp_uv.x2 = uv3;
    tmp_uv.y2 = uv4;

	*rect = tmp_rect;
	*uv = tmp_uv;
}