#include "main.h"
#include "../core/core.h"
#include "../network/network.h"
#include "gui.h"
#include "window.h"

extern window *pMainWindow;

int mainmenu::g_iCurrentMenuItemSel = 0;
float mainmenu::m_iXOff, mainmenu::m_iYOff;
float mainmenu::m_iMenuSizeX, mainmenu::m_iMenuSizeY;
bool mainmenu::g_bMenuActive = true;

#define NATIVE_RES_X    1280.0f
#define NATIVE_RES_Y    1024.0f

#define NATIVE_BG_X     1280.0f
#define NATIVE_BG_Y     649.0f

#define NATIVE_LOGO_X     1058.0f
#define NATIVE_LOGO_Y     540.0f

#define CORE_MTA_MENUITEMS_START_X  0.168

#define CORE_MTA_BG_MAX_ALPHA       1.00f   //ACHTUNG: Set to 1 for now due to GTA main menu showing through (no delay inserted between Entering game... and loading screen)
#define CORE_MTA_BG_INGAME_ALPHA    0.90f
#define CORE_MTA_FADER              0.05f // 1/20
#define CORE_MTA_FADER_CREDITS      0.01f

#define CORE_MTA_HOVER_SCALE        1.0f
#define CORE_MTA_NORMAL_SCALE       0.6f
#define CORE_MTA_HOVER_ALPHA        1.0f
#define CORE_MTA_NORMAL_ALPHA       0.6f

#define CORE_MTA_HIDDEN_ALPHA       0.0f
#define CORE_MTA_DISABLED_ALPHA     0.4f
#define CORE_MTA_ENABLED_ALPHA      1.0f

#define CORE_MTA_ANIMATION_TIME     200
#define CORE_MTA_MOVE_ANIM_TIME     600

#define MENU_ITEM_QUICK_CONNECT     1
#define MENU_ITEM_SERVER_BROWSER    2
#define MENU_ITEM_HOST_GAME         3
#define MENU_ITEM_MAP_EDITOR        4
#define MENU_ITEM_SETTINGS          5
#define MENU_ITEM_ABOUT             6
#define MENU_ITEM_EXIT              7

typedef struct ScreenVector {
    float fX, fY, fI;
};

RwTexture* bg_texture = 0;
RwTexture* bglogo_texture = 0;

void mainmenu::initialise()
{
    // Adjust window size to resolution
    float ScreenSizeX = RsGlobal->maximumWidth;
    float ScreenSizeY = RsGlobal->maximumHeight;

    LogFile("screen vector > x: %f y: %f", ScreenSizeX, ScreenSizeY);

    int iBackgroundX = 0;
    int iBackgroundY = 0;
    int iBackgroundSizeX = ScreenSizeX;
    int iBackgroundSizeY;

    // First let's work out our x and y offsets
    if (ScreenSizeX > ScreenSizeY)            // If the monitor is a normal landscape one
    {
        float iRatioSizeY = ScreenSizeY / NATIVE_RES_Y;
        m_iMenuSizeX = NATIVE_RES_X * iRatioSizeY;
        m_iMenuSizeY = ScreenSizeY;
        m_iXOff = (ScreenSizeX - m_iMenuSizeX) * 0.5f;
        m_iYOff = 0;

        float iRatioSizeX = ScreenSizeX / NATIVE_RES_X;
        iBackgroundSizeX = ScreenSizeX;
        iBackgroundSizeY = NATIVE_BG_Y * iRatioSizeX;
    }
    else            // Otherwise our monitor is in a portrait resolution, so we cant fill the background by y
    {
        float iRatioSizeX = ScreenSizeX / NATIVE_RES_X;
        m_iMenuSizeY = NATIVE_RES_Y * iRatioSizeX;
        m_iMenuSizeX = ScreenSizeX;
        m_iXOff = 0;
        m_iYOff = (ScreenSizeY - m_iMenuSizeY) * 0.5f;

        iBackgroundY = m_iYOff;
        iBackgroundSizeX = m_iMenuSizeX;
        iBackgroundSizeY = NATIVE_BG_Y * iRatioSizeX;
    }

    bg_texture = (RwTexture*)utilities::loadTextureFromDB("txd", "background");
    bglogo_texture = (RwTexture*)utilities::loadTextureFromDB("txd", "background_logo");
}

typedef struct LOGOVector {
    float fX, fY;
};

void mainmenu::draw()
{
    if(!mainmenu::g_bMenuActive) {
        return;
    }

    ImVec2 vecWindowInfoPos = ImVec2(pMainWindow->g_fPosX, pMainWindow->g_fPosY);
    ImVec2 vecWindowInfoSize = ImVec2(pMainWindow->g_fPosX + pMainWindow->g_fSizeX, pMainWindow->g_fPosY + pMainWindow->g_fSizeY);
    
    stRect rect;
    stfRect uv;

    // update rw state 
    RwRenderStateSet(rwRENDERSTATETEXTUREFILTER, (void*)rwFILTERLINEAR);

    // background texture
    if(bg_texture) 
    {
        ImGuiWrapper::CreateSTFRect(&rect, &uv, 0.0f, 0.0f, static_cast<float>(RsGlobal->maximumWidth), static_cast<float>(RsGlobal->maximumHeight), 0.0f, 0.0f, 1.0f, 1.0f);
	    ImGuiWrapper::DrawRaster(&rect, 0xFFFFFFFF, bg_texture->raster, &uv);
    }

    // background fill
    ImGuiWrapper::CreateSTFRect(&rect, &uv, 0.0f, 0.0f, static_cast<float>(RsGlobal->maximumWidth), static_cast<float>(RsGlobal->maximumHeight), 0.0f, 0.0f, 1.0f, 1.0f);
	ImGuiWrapper::DrawRaster(&rect, 0xB5000000);

    // logo image
	LOGOVector logoSize = {NATIVE_LOGO_X+(RsGlobal->maximumWidth / 4), (NATIVE_LOGO_Y / NATIVE_RES_Y) * m_iMenuSizeY};
    LOGOVector logoPos = {(RsGlobal->maximumWidth / 2) - (logoSize.fX / 2), 0.365f * m_iMenuSizeY - logoSize.fY / 2};

    if(bglogo_texture) 
    {
        float f1W = RsGlobal->maximumWidth * 0.22;
	    float f2W = RsGlobal->maximumWidth - f1W;

        ImGuiWrapper::CreateSTFRect(&rect, &uv, f1W, logoPos.fY - 60.0f, f2W, logoSize.fY - 60.0f, 0.0f, 0.0f, 1.0f, 1.0f);
	    ImGuiWrapper::DrawRaster(&rect, 0xFFFFFFFF, bglogo_texture->raster, &uv);
    }

    // menu items
    ImVec2 vecTextInfo = ImVec2(RsGlobal->maximumWidth * 0.27, (pMainWindow->g_fPosY + pMainWindow->g_fSizeY/2) - ImGui::CalcTextSize("QWERTYABOPASNBEQEW").y/2);

    float fSelectFontSizeMultiply = 1.25f;
    float fSizeOffset = 20.0f;
    bool bTabSnapEffect = false;
    bool bUseZ = true;
    bool bUseOutline = false;

    pMainWindow->setTextColor(ImColor(157, 157, 155, 255));

    if(mainmenu::g_iCurrentMenuItemSel == 1) {
        pMainWindow->setTextColor(ImColor(255, 255, 255, 255));
    }

    pMainWindow->addText(mainmenu::g_iCurrentMenuItemSel == 1 ? pMainWindow->g_fFontSize * fSelectFontSizeMultiply : pMainWindow->g_fFontSize, pMainWindow->g_pFont, vecTextInfo, pMainWindow->g_textColor, bUseOutline, mainmenu::g_iCurrentMenuItemSel == 1 && bTabSnapEffect ? "\tQUICK CONNECT" : "QUICK CONNECT");
    vecTextInfo.y += mainmenu::g_iCurrentMenuItemSel == 1 && bUseZ ? pMainWindow->g_fFontSize * fSelectFontSizeMultiply + fSizeOffset : pMainWindow->g_fFontSize + fSizeOffset;

    pMainWindow->setTextColor(ImColor(157, 157, 155, 255));

    if(mainmenu::g_iCurrentMenuItemSel == 2) {
        pMainWindow->setTextColor(ImColor(255, 255, 255, 255));
    }

    pMainWindow->addText(mainmenu::g_iCurrentMenuItemSel == 2 ? pMainWindow->g_fFontSize * fSelectFontSizeMultiply: pMainWindow->g_fFontSize, pMainWindow->g_pFont, vecTextInfo, pMainWindow->g_textColor, bUseOutline, mainmenu::g_iCurrentMenuItemSel == 2 && bTabSnapEffect ? "\tSERVER BROWSER" : "SERVER BROWSER");
    vecTextInfo.y += mainmenu::g_iCurrentMenuItemSel == 2 && bUseZ ? pMainWindow->g_fFontSize * fSelectFontSizeMultiply + fSizeOffset : pMainWindow->g_fFontSize + fSizeOffset;

    pMainWindow->setTextColor(ImColor(157, 157, 155, 255));

    if(mainmenu::g_iCurrentMenuItemSel == 3) {
        pMainWindow->setTextColor(ImColor(255, 255, 255, 255));
    }

    pMainWindow->addText(mainmenu::g_iCurrentMenuItemSel == 3 ? pMainWindow->g_fFontSize * fSelectFontSizeMultiply : pMainWindow->g_fFontSize, pMainWindow->g_pFont, vecTextInfo, pMainWindow->g_textColor, bUseOutline, mainmenu::g_iCurrentMenuItemSel == 3 && bTabSnapEffect ? "\tHOST GAME" : "HOST GAME");
    vecTextInfo.y += mainmenu::g_iCurrentMenuItemSel == 3 && bUseZ ? pMainWindow->g_fFontSize * fSelectFontSizeMultiply + fSizeOffset : pMainWindow->g_fFontSize + fSizeOffset;

    pMainWindow->setTextColor(ImColor(157, 157, 155, 255));

    if(mainmenu::g_iCurrentMenuItemSel == 4) {
        pMainWindow->setTextColor(ImColor(255, 255, 255, 255));
    }

    pMainWindow->addText(mainmenu::g_iCurrentMenuItemSel == 4 ? pMainWindow->g_fFontSize * fSelectFontSizeMultiply : pMainWindow->g_fFontSize, pMainWindow->g_pFont, vecTextInfo, pMainWindow->g_textColor, bUseOutline, mainmenu::g_iCurrentMenuItemSel == 4 && bTabSnapEffect ? "\tMAP EDITOR" : "MAP EDITOR");
    vecTextInfo.y += mainmenu::g_iCurrentMenuItemSel == 4 && bUseZ ? pMainWindow->g_fFontSize * fSelectFontSizeMultiply + fSizeOffset : pMainWindow->g_fFontSize + fSizeOffset;

    pMainWindow->setTextColor(ImColor(157, 157, 155, 255));

    if(mainmenu::g_iCurrentMenuItemSel == 5) {
        pMainWindow->setTextColor(ImColor(255, 255, 255, 255));
    }

    pMainWindow->addText(mainmenu::g_iCurrentMenuItemSel == 5 ? pMainWindow->g_fFontSize * fSelectFontSizeMultiply : pMainWindow->g_fFontSize, pMainWindow->g_pFont, vecTextInfo, pMainWindow->g_textColor, bUseOutline, mainmenu::g_iCurrentMenuItemSel == 5 && bTabSnapEffect ? "\tSETTINGS" : "SETTINGS");
    vecTextInfo.y += mainmenu::g_iCurrentMenuItemSel == 5 && bUseZ ? pMainWindow->g_fFontSize * fSelectFontSizeMultiply + fSizeOffset : pMainWindow->g_fFontSize + fSizeOffset;

    pMainWindow->setTextColor(ImColor(157, 157, 155, 255));

    if(mainmenu::g_iCurrentMenuItemSel == 6) {
        pMainWindow->setTextColor(ImColor(255, 255, 255, 255));
    }

    pMainWindow->addText(mainmenu::g_iCurrentMenuItemSel == 6 ? pMainWindow->g_fFontSize * fSelectFontSizeMultiply : pMainWindow->g_fFontSize, pMainWindow->g_pFont, vecTextInfo, pMainWindow->g_textColor, bUseOutline, mainmenu::g_iCurrentMenuItemSel == 6 && bTabSnapEffect ? "\tABOUT" : "ABOUT");
    vecTextInfo.y += mainmenu::g_iCurrentMenuItemSel == 6 && bUseZ ? pMainWindow->g_fFontSize * fSelectFontSizeMultiply + fSizeOffset : pMainWindow->g_fFontSize + fSizeOffset;

    pMainWindow->setTextColor(ImColor(157, 157, 155, 255));

    if(mainmenu::g_iCurrentMenuItemSel == 7) {
        pMainWindow->setTextColor(ImColor(255, 255, 255, 255));
    }

    pMainWindow->addText(mainmenu::g_iCurrentMenuItemSel == 7 ? pMainWindow->g_fFontSize * fSelectFontSizeMultiply : pMainWindow->g_fFontSize, pMainWindow->g_pFont, vecTextInfo, pMainWindow->g_textColor, bUseOutline, mainmenu::g_iCurrentMenuItemSel == 7 && bTabSnapEffect ? "\tEXIT" : "EXIT");
}

void mainmenu::pushTouch(float x, float y)
{
    // LOG("[PUSH] MainMenu window | %f | %f", x, y);

    if(y > 500.0f && y < 575.0f) {
        mainmenu::g_iCurrentMenuItemSel = 1;
    }
    else if(y > 575.0f && y < 650.0f) {
        mainmenu::g_iCurrentMenuItemSel = 2;
    }
    else if(y > 650.0f && y < 725.0f) {
        mainmenu::g_iCurrentMenuItemSel = 3;
    }
    else if(y > 725.0f && y < 800.0f) {
        mainmenu::g_iCurrentMenuItemSel = 4;
    }
    else if(y > 800.0f && y < 875.0f) {
        mainmenu::g_iCurrentMenuItemSel = 5;
    }
    else if(y > 875.0f && y < 950.0f) {
        mainmenu::g_iCurrentMenuItemSel = 6;
    }
    else if(y > 950.0f && y < 1025.0f) {
        mainmenu::g_iCurrentMenuItemSel = 7;
    }
    else
    {
        mainmenu::g_iCurrentMenuItemSel = 0;
    }
}

void mainmenu::toggleRender()
{
    mainmenu::g_bMenuActive ^= true;
    pMainWindow->setPadLocked(mainmenu::g_bMenuActive);
}

void mainmenu::processItem(int id)
{
    switch(id)
    {
       case MENU_ITEM_QUICK_CONNECT: 
       {
           mainmenu::g_bMenuActive = false;
           pMainWindow->setPadLocked(false);
           network::Connect("51.77.68.54", 2020, nullptr);
           break;
       }

       case MENU_ITEM_SERVER_BROWSER: {
           break;
       }

       case MENU_ITEM_HOST_GAME: {
           break;
       }

       case MENU_ITEM_MAP_EDITOR: {
           break;
       }

       case MENU_ITEM_SETTINGS: {
           break;
       }

       case MENU_ITEM_ABOUT: {
           break;
       }

       case MENU_ITEM_EXIT: {
           break;
       }

    }
}

void mainmenu::popTouch(float x, float y)
{
    // LOG("[POP] MainMenu window | %f | %f", x, y);

    mainmenu::processItem(mainmenu::g_iCurrentMenuItemSel);

    mainmenu::g_iCurrentMenuItemSel = 0;
}

void mainmenu::moveTouch(float x, float y)
{
    // LOG("[MOVE] MainMenu window | %f | %f", x, y);

    if(y > 500.0f && y < 575.0f) {
        mainmenu::g_iCurrentMenuItemSel = 1;
    }
    else if(y > 575.0f && y < 650.0f) {
        mainmenu::g_iCurrentMenuItemSel = 2;
    }
    else if(y > 650.0f && y < 725.0f) {
        mainmenu::g_iCurrentMenuItemSel = 3;
    }
    else if(y > 725.0f && y < 800.0f) {
        mainmenu::g_iCurrentMenuItemSel = 4;
    }
    else if(y > 800.0f && y < 875.0f) {
        mainmenu::g_iCurrentMenuItemSel = 5;
    }
    else if(y > 875.0f && y < 950.0f) {
        mainmenu::g_iCurrentMenuItemSel = 6;
    }
    else if(y > 950.0f && y < 1025.0f) {
        mainmenu::g_iCurrentMenuItemSel = 7;
    }
    else
    {
        mainmenu::g_iCurrentMenuItemSel = 0;
    }
}
