#ifndef MAINMENU_H
#define MAINMENU_H

class mainmenu
{
private:
    static bool g_bMenuActive;
    static int g_iCurrentMenuItemSel;
    static float m_iXOff, m_iYOff, m_iMenuSizeX, m_iMenuSizeY;
public:
    static void initialise();
    static void draw();
    static void pushTouch(float, float);
    static void popTouch(float, float);
    static void moveTouch(float, float);
    static void processItem(int);
    static void toggleRender();
};

#endif // MAINMENU_H