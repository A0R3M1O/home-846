

**Build Instructions**
**[Windows]**

| Prerequisites | 
| ------ | 
| Visual Studio Code | 
| Android NDK r16b | 


1) Download the required NDK and place it in the 'ndk' folder, next to your project. 

    It should turn out like this:

    .../your_project/jni/*
    
    .../ndk/android-ndk-r16b/*


2) Сreate the 'jni' folder (do as showed in the first step) and put the files from the 'src' folder in the archive into it.
3) Execute 'build.bat' in '/jni/' folder from terminal. (Steps: Terminal > New terminal > ./build.bat)
